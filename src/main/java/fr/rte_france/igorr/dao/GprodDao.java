package fr.rte_france.igorr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.rte_france.igorr.entity.GroupeProduction;


public interface GprodDao extends JpaRepository<GroupeProduction, Long>{

}
