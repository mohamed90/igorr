package fr.rte_france.igorr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.rte_france.igorr.entity.Ce;

public interface CeDao extends JpaRepository<Ce, Long>{

}

