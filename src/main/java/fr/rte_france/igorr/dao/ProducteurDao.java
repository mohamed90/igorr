package fr.rte_france.igorr.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.rte_france.igorr.entity.Producteur;

@Transactional(propagation=Propagation.REQUIRED)
public interface ProducteurDao extends JpaRepository<Producteur, Long>{
	
}
