package fr.rte_france.igorr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.rte_france.igorr.entity.Ce;
import fr.rte_france.igorr.entity.Gmr;

public interface GmrDao extends JpaRepository<Gmr, Long>{

}
