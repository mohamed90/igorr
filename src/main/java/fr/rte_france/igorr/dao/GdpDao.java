package fr.rte_france.igorr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.rte_france.igorr.entity.Gdp;
import fr.rte_france.igorr.entity.Gmr;

public interface GdpDao extends JpaRepository<Gdp, Long> {

}
