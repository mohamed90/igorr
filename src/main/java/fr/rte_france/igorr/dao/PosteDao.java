package fr.rte_france.igorr.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.rte_france.igorr.entity.Poste;
import fr.rte_france.igorr.entity.Poste_Igorr;

@Transactional(propagation=Propagation.REQUIRED)
public interface PosteDao extends JpaRepository<Poste_Igorr, Long>{
	
}

