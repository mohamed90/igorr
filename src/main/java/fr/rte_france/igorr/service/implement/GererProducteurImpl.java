package fr.rte_france.igorr.service.implement;

import javax.annotation.Resource;
import javax.transaction.Transactional;


import org.springframework.stereotype.Service;

import fr.rte_france.igorr.dao.GprodDao;
import fr.rte_france.igorr.dao.ProducteurDao;
import fr.rte_france.igorr.entity.GroupeProduction;
import fr.rte_france.igorr.entity.Producteur;
import fr.rte_france.igorr.entity.ProducteurType;
import fr.rte_france.igorr.service.GererProducteur;
import java.util.List;

@Service
public class GererProducteurImpl implements GererProducteur{
	
	@Resource
	private ProducteurDao producteurDao;
	@Resource
	private GprodDao gprodDao;
	@Override
	@Transactional
	public List <Producteur> getProducteurs() {
		// TODO Auto-generated method stub
		
		return producteurDao.findAll();
	}
	@Override
	@Transactional
	public GroupeProduction getProducteurById(Long id) { //interface dans dao qui devien une methode producteur dao
		return gprodDao.findOne(id);
	}
	@Override
	@Transactional
	public List<GroupeProduction> getGroupeProd() {
		return gprodDao.findAll();
	}
	public ProducteurType[] getAlltype(){
		
		return ProducteurType.values();
	}
	@Override
	@Transactional
	public GroupeProduction saveProd(GroupeProduction group){
		
		return gprodDao.save(group);
	}
/*	@Override
	@Transactional
	public GroupeProduction saveProd(GroupeProduction group){
		
		return gprodDao.save(group);
	}*/
	
	@Override
	@Transactional
	public void deleteGroup(long id){
		
		 gprodDao.delete(id);
		 
	}
	}



