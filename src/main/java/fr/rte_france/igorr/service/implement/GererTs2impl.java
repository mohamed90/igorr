package fr.rte_france.igorr.service.implement;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.rte_france.igorr.entity.DiagnosticReseau;
import fr.rte_france.igorr.entity.DiagnostiqueProducteur;
import fr.rte_france.igorr.entity.Ouvrages;
import fr.rte_france.igorr.entity.RUV;
import fr.rte_france.igorr.service.GererDiagnosticProd;
import fr.rte_france.igorr.service.GererDiagnosticReseau;
import fr.rte_france.igorr.service.GererTs2;
import helpers.Ts2;

@Service
public class GererTs2impl implements GererTs2 {
	String Ts2 = new String("TS2");
	String Ts2P = new String("TS2P");
	@Autowired
	private GererDiagnosticProd serviveDiaProd;
	@Autowired
	private GererDiagnosticReseau serviceDiaReso;
	@Override
	@Transactional
	public List<Ts2> getListreseau(List<DiagnostiqueProducteur> list){
		Ts2 gTs2;
		 List <Ts2> listGestionTs2s = new ArrayList<Ts2>();
		 List<String> liste =new ArrayList<String>();
			List<DiagnosticReseau> listes = serviceDiaReso.getList();
		
		
			for (DiagnostiqueProducteur diaFilter: list){
				int nbrTs2 = list.size();
				List<RUV> Lruv = diaFilter.getPatrimoineProducteur().getRuvs();
				String nameGroup = diaFilter.getPatrimoineProducteur().getName();
				String Ts = diaFilter.getTs();
				for(RUV r: Lruv){
					
					 Set <Ouvrages> O =  r.getOuvrages();
					 String scenario = new String(r.getName());
					 List<String> resoList = new ArrayList<String>();
					 for (Ouvrages o:O){
						 
						 for (DiagnosticReseau di : listes){
							 
							 if ((o.getId()==di.getOuvrage().getId()) && (di.getScenario().equals(scenario))){
								
								
								 resoList.add(di.getEnsOuvrage());
								 
							 }
						 }
					 }
					
					 gTs2 = new Ts2(Ts+" de"+" "+nameGroup,r.getName(),resoList,nbrTs2);
					
					  listGestionTs2s.add(gTs2);
					 
		      }
				
	   }
			return listGestionTs2s;
	}
	@Override
	@Transactional
	public int getNbreTs2(){
		List<DiagnostiqueProducteur> filterTs2= this.getListTs2();
		int nbr = filterTs2.size();
		return nbr;
	}
	@Override
	@Transactional
	public List<DiagnostiqueProducteur> getListTs2(){
		List<DiagnostiqueProducteur> liste = serviveDiaProd.getList();
		// System.out.println("ts:jkgjkfhglkhggl");
		List<DiagnostiqueProducteur> filterTs2= new ArrayList<DiagnostiqueProducteur>();
		// System.out.println("ts:jkgjkfhglkhggl");
             for(DiagnostiqueProducteur dia : liste){
            	 
			     if(dia.getTs().equals(Ts2) || dia.getTs().equals(Ts2P)) {
				
				      
				      filterTs2.add(dia);
			
			     }
			
		    }  
             for (DiagnostiqueProducteur d : filterTs2){
            	 System.out.println("ts:"+ d.getGroupe());
             }
             return filterTs2;
		
	}
}

