package fr.rte_france.igorr.service.implement;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import fr.rte_france.igorr.dao.DiagnosticReseauDao;
import fr.rte_france.igorr.dao.DiagnostiqueProducteurDao;
import fr.rte_france.igorr.entity.DiagnosticReseau;
import fr.rte_france.igorr.service.GererDiagnosticReseau;
@Service
public class GererDiagnosticReseauImpl implements GererDiagnosticReseau{
	@Resource
	private DiagnosticReseauDao diaReseau;
	 public List<DiagnosticReseau>getList(){
		 return diaReseau.findAll();
	 }
}
