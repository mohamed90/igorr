package fr.rte_france.igorr.service.implement;

import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import fr.rte_france.igorr.dao.CeDao;
import fr.rte_france.igorr.dao.GdpDao;
import fr.rte_france.igorr.dao.GmrDao;
import fr.rte_france.igorr.dao.GprodDao;
import fr.rte_france.igorr.dao.OuvragesDao;
import fr.rte_france.igorr.dao.RuvDao;
import fr.rte_france.igorr.entity.Ce;
import fr.rte_france.igorr.entity.Gdp;
import fr.rte_france.igorr.entity.Gmr;
import fr.rte_france.igorr.entity.Ouvrages;
import fr.rte_france.igorr.entity.RUV;
import fr.rte_france.igorr.service.GererPatrimoine;

@Service
public class GererPatrimoineImpl implements GererPatrimoine {
	@Resource
	private CeDao ceDao;
	@Resource
	private GmrDao gmrDao;
	@Resource
	private GdpDao gdpDao;
	@Resource
	private OuvragesDao ouvragesDao;
	@Resource
	private RuvDao ruvDao;
	@Override
	@Transactional
	public List<Ce> getListce() {
		// TODO Auto-generated method stub
		
		return ceDao.findAll();
	}
	@Override
	@Transactional
	public Ce getCeById(Long id) { //interface dans dao qui devien une methode producteur dao
		return ceDao.findOne(id);
	}
	@Override
	@Transactional
	public void deleteCe(Long id) { //interface dans dao qui devien une methode producteur dao
		ceDao.delete(id);
	}
	@Override
	@Transactional
	public void deleteGdp(Long id) { //interface dans dao qui devien une methode producteur dao
		gdpDao.delete(id);
	}
	@Override
	@Transactional
	public List<Gmr> getGmrs() {
		return gmrDao.findAll();
	}
	@Override
	@Transactional
	public List<Gdp> getGdps() {
		return gdpDao.findAll();
	}
	@Override
	@Transactional
	 public List<Ouvrages> getListOuvrages(){
		 return ouvragesDao.findAll();
	 }
	@Override
	@Transactional
	 public Ouvrages getOuvragesById(Long id) {
		return ouvragesDao.findOne(id);
		}
	@Override
	@Transactional
	public void deleteOuvrage(Long id) {
		ouvragesDao.delete(id);
	}
	@Override
	@Transactional
	 public List<RUV> getListRUV() {
		 return ruvDao.findAll();
	 }
	
	
	
}
