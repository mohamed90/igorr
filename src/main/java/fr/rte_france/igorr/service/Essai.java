package fr.rte_france.igorr.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.rte_france.igorr.entity.Producteur;



public class Essai {
	public static void main(String[] args){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("igorr");
		EntityManager em = emf.createEntityManager();
	
		//create
		em.getTransaction().begin();
		Producteur p = new Producteur();
		//p.setX(123D);
		em.persist(p);
		em.getTransaction().commit();
		em.close();
		
		
		
		emf.close();
	}
}
