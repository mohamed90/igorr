package fr.rte_france.igorr.service;

import java.util.List;

import fr.rte_france.igorr.entity.DiagnosticReseau;
import fr.rte_france.igorr.entity.DiagnostiqueProducteur;

public interface GererDiagnosticReseau {
	 public List<DiagnosticReseau>getList();
}
