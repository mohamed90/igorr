package fr.rte_france.igorr.service;

import fr.rte_france.igorr.entity.Ce;

public interface GererCe {
		public Ce getCe();
		public Ce getCeById(Long id);
}
