package fr.rte_france.igorr.service;

import java.util.List;

import fr.rte_france.igorr.entity.DiagnostiqueProducteur;
import helpers.Ts2;

public interface GererTs2 {
	public List<Ts2> getListreseau(List<DiagnostiqueProducteur> list);
	public List<DiagnostiqueProducteur> getListTs2();
	public int getNbreTs2();

}
