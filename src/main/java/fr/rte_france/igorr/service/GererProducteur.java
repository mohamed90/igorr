package fr.rte_france.igorr.service;

import java.util.List;

import fr.rte_france.igorr.entity.GroupeProduction;
import fr.rte_france.igorr.entity.Producteur;
import fr.rte_france.igorr.entity.ProducteurType;

public interface GererProducteur {
	public List<Producteur> getProducteurs();
	public GroupeProduction getProducteurById(Long id);
	public List<GroupeProduction> getGroupeProd();
	public ProducteurType[] getAlltype();
	public GroupeProduction saveProd( GroupeProduction group);
	
	void deleteGroup(long id);
		
}
