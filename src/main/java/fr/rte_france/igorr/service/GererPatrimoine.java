package fr.rte_france.igorr.service;
import java.util.List;
import java.util.Set;

import fr.rte_france.igorr.entity.Ce;
import fr.rte_france.igorr.entity.Gdp;
import fr.rte_france.igorr.entity.Gmr;
import fr.rte_france.igorr.entity.Ouvrages;
import fr.rte_france.igorr.entity.RUV;

public interface GererPatrimoine {
	 public List<Gmr> getGmrs();
	  public List<Gdp> getGdps();
	  public List<Ce> getListce();
	  public Ce getCeById(Long id);
	  public List<Ouvrages> getListOuvrages();
	  public List<RUV> getListRUV();
	  public Ouvrages getOuvragesById(Long id);
	  void deleteOuvrage(Long id);
	//public void deleteCe(long id);
	  public void deleteGdp(Long id);
	void deleteCe(Long id);

}
