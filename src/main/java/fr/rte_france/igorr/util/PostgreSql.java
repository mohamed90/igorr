package fr.rte_france.igorr.util;
import java . sql . Connection ;
import java . sql . DriverManager ;
import java . sql . SQLException ; 

public class PostgreSql {
	
	 private final String url = "jdbc:postgresql://localhost/dvdrental" ;
	 private final String user = "postgres" ;
	 private final String password = "" ; 
	 
	  /**
	     * Connect to the PostgreSQL database
	     *
	     * @return a Connection object
	     */
	     public Connection connect ( ) {
	         Connection conn = null ;
	         try {
	             conn = DriverManager.getConnection ( url , user , password ) ;
	             System.out.println ( "Connected to the PostgreSQL server successfully." ) ;
	         } catch ( SQLException e ) {
	             System . out . println ( e . getMessage ( ) ) ;
	         }
	 
	         return conn ;
	     } 

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		   PostgreSql app = new PostgreSql( ) ;
	         app.connect ( ) ; 
	}

}
