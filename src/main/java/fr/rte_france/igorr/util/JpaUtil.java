package fr.rte_france.igorr.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
	static EntityManagerFactory factory ;
	
	private JpaUtil(){};
	
	static {
		System.out.println("*** Démarrage de JPA ***");
		factory = Persistence.createEntityManagerFactory("igorr");
	}

	public static EntityManagerFactory getEntityManagerFactory() {
		return factory;
	}
}