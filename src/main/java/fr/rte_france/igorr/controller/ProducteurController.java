package fr.rte_france.igorr.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import fr.rte_france.igorr.dao.ProducteurDao;
import fr.rte_france.igorr.entity.DiagnosticReseau;
import fr.rte_france.igorr.entity.DiagnostiqueProducteur;
import fr.rte_france.igorr.entity.GroupeProduction;
import fr.rte_france.igorr.entity.Producteur;
import fr.rte_france.igorr.entity.ProducteurType;
import fr.rte_france.igorr.service.GererDiagnosticProd;
import fr.rte_france.igorr.service.GererProducteur;
import helpers.Diagnosticprod;

@RestController
@RequestMapping("/producteurs")
public class ProducteurController {
	@Autowired
	private GererProducteur gererProducteur;
	@Autowired
	private GererDiagnosticProd prodgestion;
	 
	
	/*ConfigurableApplicationContext spring = new ClassPathXmlApplicationContext("spring.xml");
	ProducteurDao producteurDao = spring.getBean(ProducteurDao.class);*/
	
	/*@RequestMapping(value="/{id}", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
	@Transactional
	public ResponseEntity<Producteur> getProducteursbyId(@PathVariable Long id) {
		Producteur producteur = producteurDao.findOne(id);
		return new ResponseEntity<>(producteur, HttpStatus.OK);
	}*/
	
	@RequestMapping(value="/", method = RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
	@Transactional
	@CrossOrigin(origins = "http://localhost:4200")
	public ResponseEntity<Set<Diagnosticprod>> getProducteurs() {
		 List<GroupeProduction> producteurs = gererProducteur.getGroupeProd();
		 Set <Diagnosticprod> incidents = new HashSet<Diagnosticprod>();
		//DiagnostiqueProducteur(String centre_exploitation,String site,String groupe, String etat, String ts, String commentaire)
		for (GroupeProduction group : producteurs){
			Diagnosticprod reseau = new Diagnosticprod();
			reseau.setId(group.getId());
			reseau.setCentre_exploitation(group.getRegion());
			reseau.setGroupe(group.getName());
			reseau.setPoste(group.getLibelle_poste());
			reseau.setType(group.getTypeProd().name());
			incidents.add(reseau);
			
		}
		//diagnostic prod a afficher
	    return new ResponseEntity<>(incidents, HttpStatus.OK);
	}
	
	@RequestMapping(value="/type", method = RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
	@Transactional
	@CrossOrigin(origins = "http://localhost:4200")
	public ResponseEntity<ProducteurType[]> getType(){
		ProducteurType[] type = gererProducteur.getAlltype();
	 return new ResponseEntity<>(type, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.POST)
	@Transactional
	@CrossOrigin(origins = "http://localhost:4200")
	public GroupeProduction saveGroup(@RequestBody GroupeProduction group) {
		return gererProducteur.saveProd(group);
		
    }
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Transactional
	@CrossOrigin(origins = "http://localhost:4200")
	public GroupeProduction getGroup(@PathVariable("id") Long id) {
		return gererProducteur.getProducteurById(id);
	}
	@Transactional
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method = RequestMethod.PUT)
	public GroupeProduction updateGroup(@RequestBody GroupeProduction user) {
		return new GroupeProduction();
	}
	@Transactional
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean deleteUser(@PathVariable("id") Long id) {
		GroupeProduction gr = this.getGroup(id);
		List<DiagnostiqueProducteur>ldia= prodgestion.getList();
		for (DiagnostiqueProducteur ld : ldia){
			if (ld.getPatrimoineProducteur()==gr){
				ld.setId(null);
			}
		}
		gererProducteur.deleteGroup(id);
		return true;
	}
}
