package fr.rte_france.igorr.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.rte_france.igorr.dao.DiagnostiqueProducteurDao;
import fr.rte_france.igorr.entity.DiagnosticReseau;
import fr.rte_france.igorr.entity.DiagnostiqueProducteur;
import fr.rte_france.igorr.entity.Ouvrages;
import fr.rte_france.igorr.entity.RUV;
import fr.rte_france.igorr.service.GererDiagnosticProd;
import fr.rte_france.igorr.service.GererDiagnosticReseau;
import fr.rte_france.igorr.service.GererTs2;
import helpers.DiagnotisqueOuvrage;
import helpers.Ts2;

@RestController
@RequestMapping("/gestionts2")
public class GestionTs2 {
	String nameTs2=""; String ts2="";Ts2 gTs2;

	@Autowired
	private GererTs2 serviceTs2;
	@RequestMapping(value="/", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	@CrossOrigin(origins = "http://localhost:4200")
	@Transactional
	public ResponseEntity<List<Ts2>> getDiagnotic() {
 List<DiagnostiqueProducteur> listes = serviceTs2.getListTs2();
	  

	   
	   List<Ts2> resoLists = serviceTs2.getListreseau(listes);
	
		 return new ResponseEntity<>(resoLists , HttpStatus.OK);
	}
	@RequestMapping(value="/name", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	@CrossOrigin(origins = "http://localhost:4200")
	@Transactional
	public List<String> getNameGroupTs2(){
		
		 List<DiagnostiqueProducteur> listes = serviceTs2.getListTs2();
		 List<String> list = new ArrayList<String>();
		 for (DiagnostiqueProducteur dia : listes){
			 
			 list.add(dia.getGroupe());
			 
			 
		 }
		 return list;
	}
	@RequestMapping(value="/nombre", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	@CrossOrigin(origins = "http://localhost:4200")
	@Transactional
	public int getnbrets2(){
		return this.serviceTs2.getNbreTs2();
	}
}

