package fr.rte_france.igorr.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.rte_france.igorr.entity.RUV;
import fr.rte_france.igorr.service.GererPatrimoine;
import helpers.DataforEdges;
@RestController
@RequestMapping("/ruv")
public class RuvController {
	@Autowired
	private GererPatrimoine gererPatrimoine;
   
	@RequestMapping(value="/name", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	@CrossOrigin(origins = "http://localhost:4200")
	@Transactional
	public ResponseEntity<List<DataforEdges>> getNameRenvoi() {
         List< DataforEdges> data = new ArrayList< DataforEdges>();
     	List<RUV> scenarios = gererPatrimoine.getListRUV(); 
		
     	for (RUV r:scenarios){
			System.out.println(r.getName());
			
			 DataforEdges nameOuvrage = new DataforEdges();
			 nameOuvrage.setId(r.getId());
			 nameOuvrage.setName(r.getName());
			 data.add(nameOuvrage);
			
     	}
		 return new ResponseEntity<>(data, HttpStatus.OK);
		
	}
}
