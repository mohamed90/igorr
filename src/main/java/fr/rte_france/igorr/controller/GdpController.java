package fr.rte_france.igorr.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.rte_france.igorr.entity.Gdp;
import fr.rte_france.igorr.service.GererPatrimoine;
import helpers.DataforEdges;
@RestController
@RequestMapping("/gdp")
public class GdpController { 
	@Autowired
	private GererPatrimoine gererPatrimoine;
	
	@RequestMapping(value="/", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	@CrossOrigin(origins = "http://localhost:4200")
	@Transactional
	public ResponseEntity<List<DataforEdges>> getGdpForOuvrage() {
		 List <Gdp> gdps = gererPatrimoine.getGdps();
		 List< DataforEdges> data = new ArrayList<>();
		 for (Gdp gf : gdps){
			 DataforEdges gdp = new DataforEdges();
			 gdp.setName(gf.getLib_gdp());
			 gdp.setId(gf.getId_gdp());
			 data.add(gdp);
		 }
		
		 return new ResponseEntity<>(data, HttpStatus.OK);
	}
}
