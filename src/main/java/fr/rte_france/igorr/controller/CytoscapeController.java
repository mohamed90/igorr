package fr.rte_france.igorr.controller;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import helpers.DataforEdges;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.rte_france.igorr.dao.ProducteurDao;
import fr.rte_france.igorr.entity.Ce;
import fr.rte_france.igorr.entity.DiagnosticReseau;
import fr.rte_france.igorr.entity.DiagnostiqueProducteur;
import fr.rte_france.igorr.entity.Gdp;
import fr.rte_france.igorr.entity.Gmr;
import fr.rte_france.igorr.entity.GroupeProduction;
import fr.rte_france.igorr.entity.Ligne;
import fr.rte_france.igorr.entity.Niveau_Tension;
import fr.rte_france.igorr.entity.Ouvrages;
import fr.rte_france.igorr.entity.Poste_Igorr;
import fr.rte_france.igorr.entity.Producteur;
import fr.rte_france.igorr.entity.ProducteurType;
import fr.rte_france.igorr.entity.RUV;
import fr.rte_france.igorr.entity.Site;
import fr.rte_france.igorr.service.GererDiagnosticReseau;
import fr.rte_france.igorr.service.GererPatrimoine;
import fr.rte_france.igorr.service.GererProducteur;

import helpers.CytoscapePosition;
import helpers.DataforEdges;
import helpers.DiagnotisqueOuvrage;

@RestController
@RequestMapping("/patrimoine")
public class CytoscapeController {
@Autowired
	private GererPatrimoine gererPatrimoine;
@Autowired
 private GererDiagnosticReseau diaReso;
   
  
	@RequestMapping(value="/", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	@CrossOrigin(origins = "http://localhost:4200")
	@Transactional
	public ResponseEntity<Set<DiagnotisqueOuvrage>> getOuvragesForDiagnoticR() {
						
						List<RUV> scenarios = gererPatrimoine.getListRUV(); 
						 Set <DiagnotisqueOuvrage> incidents = new HashSet<>();
						for (RUV r:scenarios){
							for (Ouvrages O: r.getOuvrages()){
								DiagnotisqueOuvrage incident = new DiagnotisqueOuvrage();
								incident.setId(O.getId()); //dans save base de donnee rechercher ouvrage par id
								incident.setCentre_exploitation(O.getcentre().getLib_ce());
								incident.setScenario(r.getName());
								incident.setGmr(O.getGdp_chef_de_file().getGmr().getLib_gmr());
								incident.setGdp(O.getGdp_chef_de_file().getLib_gdp());
								incident.setOuvrage(O.getNameOuvrage());
								incident.setTension(O.getTension().getVolt());
								incident.setType_ouvrage(O.getType());
								
								//equals compare l'adresse et non les champs; //new cree une nouvelle adresse
								incidents.add(incident);
							
								
							}
							
							
						}
						
						
						/* expliciter ce qui serait afficher en vu detre generé dans Postman*/
	       return new ResponseEntity<>(incidents, HttpStatus.OK);
	}
	@RequestMapping(value="/ouvrage", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	@CrossOrigin(origins = "http://localhost:4200")
	@Transactional
	public ResponseEntity<Set<DiagnotisqueOuvrage>> getOuvrages() {
						List<Ouvrages> scenarios = gererPatrimoine.getListOuvrages(); 
						 Set <DiagnotisqueOuvrage> incidents = new HashSet<>();	
						for (Ouvrages O:scenarios){
								System.out.println(O.getNameOuvrage());
								DiagnotisqueOuvrage incident = new DiagnotisqueOuvrage();
								incident.setId(O.getId()); //dans save base de donnee rechercher ouvrage par id
								incident.setCentre_exploitation(O.getcentre().getLib_ce());
								incident.setGdp(O.getGdp_chef_de_file().getLib_gdp());
								incident.setOuvrage(O.getLib_ouvrage());
								incident.setNumero_ordre(O.getNumero_ordre());
								incident.setTension(O.getTension().name());
								incident.setType_ouvrage(O.getType());
								incidents.add(incident);

								
								
							}
										
	       return new ResponseEntity<>(incidents, HttpStatus.OK);
	


	
}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Transactional
	@CrossOrigin(origins = "http://localhost:4200")
	public Ouvrages getOuvrage(@PathVariable("id") Long id) {
		
		Ouvrages ou = gererPatrimoine.getOuvragesById(id);
		System.out.print("ouvrage :"+ou.getLib_ouvrage());
		return ou;
		
	}
	@Transactional
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean deleteOuvrages(@PathVariable("id") Long id) {
		Ouvrages ou = this.getOuvrage(id);
		List<RUV> ruv = gererPatrimoine.getListRUV();
		    for (RUV r : ruv) {
		    	 r.getOuvrages().remove(ou); //ou est clé etrangere in RUV donc il faut tt d'abord y viré et apres le viré lui meme
		    	 
		    		
		    	}
		  
			List<DiagnosticReseau>ldia= diaReso.getList();
			for (DiagnosticReseau ld : ldia){
				if (ld.getOuvrage()==ou){
					ld.setOuvrage(null);
				}
			}
			
		    gererPatrimoine.deleteOuvrage(id);
		    
		return true;
	}
}


