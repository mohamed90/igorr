package fr.rte_france.igorr.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.rte_france.igorr.entity.Gmr;
import fr.rte_france.igorr.service.GererPatrimoine;
import helpers.DataforEdges;
@RestController
@RequestMapping("/gmr")
public class GmrController {
	@Autowired
	private GererPatrimoine gererPatrimoine;
	@RequestMapping(value="/", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	@CrossOrigin(origins = "http://localhost:4200")
	@Transactional
	public ResponseEntity<List<DataforEdges>> getGmrForOuvrage() {
		 List <Gmr> gmrs = gererPatrimoine.getGmrs();
		 List< DataforEdges> data = new ArrayList< DataforEdges>();
		 for (Gmr gf : gmrs){
			 DataforEdges gmr = new DataforEdges();
			 gmr.setName(gf.getLib_gmr());
			 gmr.setId(gf.getId_gmr());
			 data.add(gmr);
		 }
		
		 return new ResponseEntity<>(data, HttpStatus.OK);
	}
}
