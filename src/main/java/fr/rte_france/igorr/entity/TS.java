package fr.rte_france.igorr.entity;

public enum TS {
	//AFFICHER PAR ORDER	
		INCONNU("Inconnu", 1), 
		TS1("TS1", 2), 
		TS2("TS2", 3), 
		TS2P("TS2P", 4),
		NONTS1TS2("NON TS1 TS",5);
		
		
		
		final String value;
		final int order;
		
		TS (String value, int order) { 
			
			this.value = value ;
			this.order = order;
		}
		
		@Override
		public	
		String toString() { return value ;}

		String getKey() { return name() ;}	
		
		int getOrder() { return order ;}
}
