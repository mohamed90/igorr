package fr.rte_france.igorr.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


/**
 * Entité définissant un centre d'exploitation du patrimoine
 * @author Lopez Pierre
 * @version 1.0
 */
@Entity
public class Ce {
	/* ATTRIBUTS */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id_ce;
	private String lib_ce;
	/*private Boolean modeSidre;
	private String support;*/
	//bi-directionel one-to-many vers GMR
    @OneToMany ( mappedBy ="ce") 	
	private List<Gmr> gmrs = new ArrayList<Gmr>();
	//bi-directionel one-to-many vers Producteur	
	/*@OneToMany ( mappedBy ="ce") 	
	private List<Producteur> producteurs = new ArrayList<Producteur>();*/
	
	/* CONSTRUTEURS */
	public Ce(){
		
	}
	public Ce( String libelle) {
		
		this.lib_ce = libelle;
		/*this.modeSidre = Boolean.FALSE;*/
	}
	
	/* GETTER - SETTER */
	public Long getId_ce() {
		return id_ce;
	}
	public void setId_ce(Long id) {
		this.id_ce = id;
	}
	public String getLib_ce() {
		return lib_ce;
	}
	public void setLibelle(String libelle) {
		this.lib_ce = libelle;
	}
	/*public Boolean getModeSidre() {
		return modeSidre;
	}
	public void setModeSidre(Boolean modeSidre) {
		this.modeSidre = modeSidre;
	}
	public String getSupport() {
		return support;
	}
	public void setSupport(String support) {
		this.support = support;
	}*/
	public List<Gmr> getGmrs() {
		return gmrs;
	}
	public void setGmrs(List<Gmr> gmrs) {
		this.gmrs = gmrs;
	}
	/*public List<Producteur> getProducteurs() {
		return producteurs;
	}
	public void setProducteurs(List<Producteur> producteurs) {		this.producteurs = producteurs;
	}*/
}
