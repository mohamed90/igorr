package fr.rte_france.igorr.entity;

public enum Etat {
//AFFICHER PAR ORDER	
	INCONNU("Inconnu", 1), 
	DISPONIBLE("Disponible", 2), 
	INDISPONIBLE("indisponible", 3), 
	INDISPOAVANT("indisponible avant IG", 4); 
	
	
	
	final String value;
	final int order;
	
	Etat (String value, int order) { 
		
		this.value = value ;
		this.order = order;
	}
	
	@Override
	public	
	String toString() { return value ;}

	String getKey() { return name() ;}	
	
	int getOrder() { return order ;}
}