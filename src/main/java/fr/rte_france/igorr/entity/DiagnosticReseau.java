package fr.rte_france.igorr.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: DiagnosticReseau
 *
 */
@Entity

public class DiagnosticReseau implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String centre_exploitation;
	/*private String Gmr_chef_de_file;
	private String Gdp_chef_de_file;*/
	private String Scenario;
	private String OuvrageName;
	private String Tension;
	private String Type;
	private String disponibilité;
	private String commentaire;
	@ManyToOne(fetch = FetchType.EAGER)
	private Ouvrages ouvrage;
	
	public DiagnosticReseau(){
	}
	public DiagnosticReseau(String scenario,String centre_exploitation,String ouvrage,String tension,String type,String dispo, String commentaire,Ouvrages ou) {
		this.centre_exploitation= centre_exploitation;
		this.Scenario= scenario;
		/*this.Gmr_chef_de_file= gmr;
		this.Gdp_chef_de_file = gdp;*/
		this.disponibilité = dispo;
		this.OuvrageName= ouvrage;
		this.ouvrage= ou;
		this.Tension= tension;
		this.Type=type;
		this.commentaire = commentaire;
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCentre_exploitation() {
		return centre_exploitation;
	}

	public void setCentre_exploitation(String centre_exploitation) {
		this.centre_exploitation = centre_exploitation;
	}

	/*public String getGmr() {
		return Gmr_chef_de_file;
	}

	public void setGmr(String gmr) {
		this.Gmr_chef_de_file = gmr;
	}

	public String getGdp() {
		return Gdp_chef_de_file;
	}

	public void setGdp(String gdp) {
		this.Gdp_chef_de_file = gdp;
	}*/

	public String getOuvrageName() {
		return OuvrageName;
	}

	public void setOuvrage(Ouvrages ouvrage) {
		this.ouvrage = ouvrage;
	}
	public Ouvrages getOuvrage() {
		return ouvrage;
	}

	public void setOuvrageName(String ouvrage) {
		this.OuvrageName = ouvrage;
	}
	public String getType_ouvrage() {
		return Type;
	}

	public void setType_ouvrage(String type_ouvrage) {
		this.Type = type_ouvrage;
	}

	public String getDisponibilite() {
		return disponibilité;
	}

	public void setDisponibilite(String disponibilite) {
		this.disponibilité = disponibilite;
	}
	public String getScenario() {
		return Scenario;
	}

	public void setScenario(String ruv) {
		this.Scenario = ruv;
	}
	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	

	public String getTension() {
		return Tension;
	}

	public void setTension(String tension) {
		this.Tension = tension;
	}
  public String getEnsOuvrage(){
	  
	  return getOuvrageName()+" "+getTension()+": "+getDisponibilite();
	  
	  
  }
   
}
