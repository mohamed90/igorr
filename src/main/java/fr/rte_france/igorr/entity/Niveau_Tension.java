package fr.rte_france.igorr.entity;

public enum Niveau_Tension {
	UN("1","20kV"),
	DEUX("2","<45kV"),
	TROIS("3","63kV"),
	QUATRE("4","90kV"),
	SIX("6","225kV"),
	SEPT("7","400kV"),
	SEPTSIX("76","400-225kV");
	
	final String value;
	final String volt;
	Niveau_Tension(String value,String volt) { 
		
		this.value = value ;
		this.volt = volt;
		
	}
	
	@Override
	public	
	String toString() { return value ;}
	public String getVolt() {return volt;}

	String getKey() { return name() ;}	
	
	

}
