package fr.rte_france.igorr.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: AutoTransformateur
 *
 */
@Entity
@Table(name = "AutoTransformateur")
@DiscriminatorValue(value="AT")
public class AutoTransformateur extends Ouvrages {

	
	private static final long serialVersionUID = 1L;

	public AutoTransformateur() {}
	 
	  
		
	  // private Niveau_Tension TENSION;
	
	
	 public AutoTransformateur (String LIB_OUVRAGE, Niveau_Tension TENSION,int NUMERO_ORDRE,Gdp CHEF_DE_FILE,Ce centre) 
	 {
		  this.tension = TENSION;
		  this.lib_ouvrage = LIB_OUVRAGE;
		  this.numero_ordre = NUMERO_ORDRE;
		  this.gdp_chef_de_file = CHEF_DE_FILE;
		  this.centre = centre ;
		 
	 }
	 /* GETTER - SETTER */
		
	/*	public Niveau_Tension getTension() {
			return TENSION;
		}
		public void setTension(Niveau_Tension tension) {
			this.TENSION = tension;
		}*/
	      public String getNameOuvrage(){
			return "AT".concat(getTension().toString()).concat(String.valueOf(getNumero_ordre()))+" "+ getLib_ouvrage();
		}
	      public  String getOuvrageWithNum(){
			   
			   return this.getLib_ouvrage()+" "+ this.getNumero_ordre();
			   
		   }
}
