package fr.rte_france.igorr.entity;

import java.util.ArrayList;
import java.util.List;

//import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: GroupeProduction
 *
 */
@Entity
public class GroupeProduction {//FOR IGORR
	
/* ATTRIBUTS */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String libelle_poste;
	private String name;
	private int num;
	private String region;
    private ProducteurType typeProd;
	@ManyToMany
	private List<RUV> ruvs = new ArrayList<>();
   /* @OneToOne(fetch = FetchType.EAGER) 	
	@JoinColumn(name = "ID_poste")
    private Poste_Igorr poste_evacuation;//POSTE IGORR SURTOUT A PREVOIR
	CONSTRUCTEURS */
	public GroupeProduction(){ 
		
	}
	public GroupeProduction(String name ,String libelle_poste,int num ,ProducteurType type ,String region){
		/*this.id = id;*/
		this.libelle_poste = libelle_poste;
		this.name = name;
		this.num = num ;
		this.region = region ;
	
		this.typeProd = type;
		
		//this.type = type;
	}
	public GroupeProduction(String name ,String libelle_poste,ProducteurType type ,String region ){
	
		this.libelle_poste = libelle_poste;
		this.name = name;
		
		this.region = region ;
		
		
		this.typeProd = type;
	}
	public void addRuv(RUV ru){
		this.ruvs.add(ru); /*ru.getGroup().add(this);*/
	}
	/* GETTER - SETTER */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String id) {
		this.name = id;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getLibelle_poste() {
		return libelle_poste;
	}
	public void setLibelle_poste(String libelle) {
		this.libelle_poste = libelle;
		
	}
	
	public  ProducteurType getTypeProd(){
	    return typeProd;	
	}
	
    public void setTypeProd(ProducteurType type) {
      this.typeProd = type;
    }
    public List<RUV> getRuvs(){
		return ruvs;
	}
	public void setRuvs(List<RUV> ruvs){
		this.ruvs = ruvs;
}
}
