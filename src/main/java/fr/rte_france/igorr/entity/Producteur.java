package fr.rte_france.igorr.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Entité définissant un producteur électrique
 * @author Lopez Pierre
 * @version 1.0
 */
@Entity
public class Producteur { // production CONFONDU TOUTE LA FRANCE
	/* ATTRIBUTS */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String libelle;
	
	private String sites ;
	private ProducteurType type;
	
	/*@OneToMany(fetch = FetchType.EAGER) 	
	private List<GroupeProduction> gprod = new ArrayList<GroupeProduction>();*/
	@OneToOne(fetch = FetchType.EAGER) 	
	@JoinColumn(name = "ID_Poste")
	private Poste poste;
	/* CONSTRUCTEURS */
	public Producteur(){
		
	}
	public Producteur(Long id, String libelle,ProducteurType type){
		this.id = id;
		this.libelle = libelle;
		this.type = type;
	}
	
	/* GETTER - SETTER */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Poste getPoste(){
		return poste;
	}
	public void setPoste(Poste poste){
		this.poste = poste;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	/*public Double getX() {
		return x;
	}
	public void setX(Double x) {
		this.x = x;
	}
	public Double getY() {
		return y;
	}
	public void setY(Double y) {
		this.y = y;
	}*/
	public ProducteurType getType() {
		return type;
	}
	public void setType(ProducteurType type) {
		this.type = type;
	}
	public String getSites() {
		return sites;
	}
	public void setSites(String ce) {
		this.sites = ce;
	}
	/*public List<GroupeProduction> getGprod() {
		return gprod;
	}
	public void setGprod(List<GroupeProduction> gprod) {
		this.gprod = gprod;
	}*/
}
