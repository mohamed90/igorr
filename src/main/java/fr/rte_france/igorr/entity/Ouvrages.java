package fr.rte_france.igorr.entity;


import java.io.Serializable;
import java.util.HashSet;

import java.util.Set;

//import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Ouvrages
 *
 */
@Entity
@Table(name = "ouvrage")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
	    name="type_ouvrage",
	    discriminatorType=DiscriminatorType.STRING
	    )
public abstract class Ouvrages implements Serializable  {

	private static final long serialVersionUID = 1L;
	/* ATTRIBUTS */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY) 	
	@JoinColumn(name = "ID_CE")
	protected Ce centre;
	@Column(name="type_ouvrage", insertable = false, updatable = false)
	protected String type;
	protected String lib_ouvrage;
	@ManyToOne(fetch = FetchType.LAZY) 	
	@JoinColumn(name = "ID_GdpCHEF_FILE")
	protected Gdp gdp_chef_de_file; 
	//private Niveau_Tension TENSION ;
	protected int numero_ordre;
	protected Niveau_Tension tension;
	@ManyToMany(mappedBy = "ouvrages")
	protected Set <RUV> ruvs = new HashSet <RUV>();
	
	/* CONSTRUCTEURS */
	public Ouvrages(){
		
	}

	public abstract String getNameOuvrage();
	public abstract String getOuvrageWithNum();
		//return getTension().getVolt()+" "+getLIB_OUVRAGE();

	/*public String getNameAT(){
		return "AT"+""+getTension().getVolt()+""+getNumero()+""+ getLIB_OUVRAGE();
		
	}
	public String getNameSelf() {
		return getLIB_OUVRAGE()+""+ "Self"+""+getNumero();
	}*/
	//helpers

	public void addRUV(RUV renvoi){
		ruvs.add(renvoi); renvoi.getOuvrages().add(this);
	}
	public Set <RUV> getRuv(){
		return ruvs;
	}
	public void setRuv(Set<RUV> ruvs) {
		this.ruvs = ruvs;
	}
	/* GETTER - SETTER */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType(){
		return type;
	}
	public int getNumero_ordre() {
		return numero_ordre;
	}
	public void setNumero_ordre(int numero) {
		this.numero_ordre = numero;
	}
	/*public String getLibelle() {
		return  lib_ouvrage;
	}
	public void setLibele(String lib) {
		this.lib_ouvrage = lib;
	}*/
	public Gdp getGdp_chef_de_file() {
		return gdp_chef_de_file; 
	}
	public void setGdp_chef_de_file(Gdp x) {
		this.gdp_chef_de_file = x;
	}
	public void setLib_ouvrage(String x) {
		this.lib_ouvrage = x;
	}
	public String getLib_ouvrage() {
		return lib_ouvrage;
	}
	/*public String toString() {
		return "name: "+getLibelle()+","+"Type :"+type_ouvrage;
	}*/
	public void setTension(Niveau_Tension x) {
		this.tension= x;
	}
	public Niveau_Tension getTension() {
		return tension;
	}
	public Ce getcentre() {
		return centre;
	}
	public void setCentre(Ce y) {
		this.centre = y;
	}

}
