package fr.rte_france.igorr.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Entité définissant un poste
 * @author Lopez Pierre
 * @version 1.0
 */
@Entity
public class Poste {
	/* ATTRIBUTS */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String libelle;
	private String idPo;
	//private Integer tension;
	/*private Double x;
	private Double y;*/
	//bi-directionel many-to-one association vers site	
	@ManyToOne(fetch = FetchType.EAGER) 	
	@JoinColumn(name = "ID_SITE")
	private Site site;
	private Niveau_Tension tension;
	/* CONSTRUCTEUR */
	public Poste(){
	}

	public Poste(Long id, String libelle, Site site, String idPo, Niveau_Tension tension/*, Double x, Double y*/) {
		this.id = id;
		this.libelle = libelle;
		this.site = site;
		this.idPo = idPo;
		this.tension = tension;
		/*this.x = x;
		this.y = y;*/
	}
	
	/* GETTER - SETTER */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getIdPo() {
		return idPo;
	}

	public void setIdPo(String idPo) {
		this.idPo = idPo;
	}

	public Niveau_Tension getTension() {
		return tension;
	}

	public void setTension(Niveau_Tension tension) {
		this.tension = tension;
	}

	/*public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}*/
}
