package fr.rte_france.igorr.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class DiagnostiqueProducteur {
	/* ATTRIBUTS */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String centre_exploitation;
	private ProducteurType type;
	private String groupe;
	private String etat;
	private String ts;
	private String commentaire;
	@ManyToOne(fetch = FetchType.EAGER)
	private GroupeProduction patrimoineProducteur;
	
	public DiagnostiqueProducteur(){
	}
	public DiagnostiqueProducteur(GroupeProduction id,String centre_exploitation,ProducteurType type,String groupe, String etat, String ts, String commentaire) {
		this.centre_exploitation= centre_exploitation;
		this.type= type;
		this.groupe = groupe;
		this.etat = etat;
		this.ts = ts;
		this.commentaire = commentaire;
		this.patrimoineProducteur = id;
	}
	public GroupeProduction  getPatrimoineProducteur() {
		return patrimoineProducteur;
	}
	public void setId( GroupeProduction id) {
		this.patrimoineProducteur= id;
	}
	public String getCentre_exploitation() {
		return centre_exploitation;
	}

	public void setCentre_exploitation(String centre_exploitation) {
		this.centre_exploitation = centre_exploitation;
	}

	public ProducteurType getType() {
		return type;
	}

	public void setType(ProducteurType type) {
		this.type = type;
	}

	public String getGroupe() {
		return groupe;
	}

	public void setGroupe(String groupe) {
		this.groupe = groupe;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	
	
}
