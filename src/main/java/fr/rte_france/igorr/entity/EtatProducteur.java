package fr.rte_france.igorr.entity;

public enum EtatProducteur {
	//AFFICHER PAR ORDER	
		INCONNU("Inconnu", 1), 
		DECLANCHE("Déclenché", 2), 
		DISPONIBLEARRET("Disponible à arret", 3), 
		VISITEANNUELLE("Visite Annuelle", 4),
		ILOTE("Iloté", 5),
		COUPLE("Couplé",6);
		
		
		
		final String value;
		final int order;
		
		EtatProducteur (String value, int order) { 
			
			this.value = value ;
			this.order = order;
		}
		
		@Override
		public	
		String toString() { return value ;}

		String getKey() { return name() ;}	
		
		int getOrder() { return order ;}
}
