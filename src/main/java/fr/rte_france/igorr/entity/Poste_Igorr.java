package fr.rte_france.igorr.entity;

//import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Poste_Igorr
 *
 */
@Entity
@Table(name = "Poste")
@DiscriminatorValue(value="POSTE")
public class Poste_Igorr extends Ouvrages {

	private static final long serialVersionUID = 1L;
		@ManyToOne(fetch = FetchType.EAGER) 	
		@JoinColumn(name = "ID_SITE")
		private Site site;
		

		public Poste_Igorr (String LIB_OUVRAGE,Site site, Niveau_Tension TENSION,Gdp CHEF_DE_FILE,Ce centre)  {
			/*this.id = id;*/
			 this.tension = TENSION;
			  this.lib_ouvrage = LIB_OUVRAGE;
		
			  this.gdp_chef_de_file=CHEF_DE_FILE;
			  this.centre = centre;
			
			this.site = site;
		}
		
		/* GETTER - SETTER */
	

	


		public Site getSite() {
			return site;
		}

		public void setSite(Site site) {
			this.site = site;
		}

		 public String getNameOuvrage(){
			 
				return getLib_ouvrage()+" "+getTension().getVolt();
			}
		
		  public  String getOuvrageWithNum(){
			   
			   return this.getLib_ouvrage();
			   
		   }

	

	public Poste_Igorr() {
		super();
	}
   
}
