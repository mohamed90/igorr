package fr.rte_france.igorr.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Self
 *
 */
@Entity
@Table(name = "self")
@DiscriminatorValue(value="SELF")
public class Self extends Ouvrages {

	
	private static final long serialVersionUID = 1L;

	public Self() {}
	
	   //private Niveau_Tension TENSION;
	
	
	 public Self (String LIB_OUVRAGE,Niveau_Tension TENSION,int NUMERO_ORDRE,Gdp CHEF_DE_FILE , Ce centre) 
	 {
		  this.tension = TENSION;
		  this.lib_ouvrage = LIB_OUVRAGE;
		  this.numero_ordre = NUMERO_ORDRE;
		  this.gdp_chef_de_file = CHEF_DE_FILE;
		  this.centre = centre;
		 
	 }
	 /* GETTER - SETTER */
		
		/*public Niveau_Tension getTension() {
			return TENSION;
		}
		public void setTension(Niveau_Tension tension) {
			this.TENSION = tension;
		}*/
	 public String getNameOuvrage() {
		 if (this.numero_ordre!=0){
			return getLib_ouvrage()+""+ "Self"+" "+getNumero_ordre();
          }
		 return getLib_ouvrage()+" "+ "Self";
}
	  public  String getOuvrageWithNum(){
		  if (this.numero_ordre!=0){
		  
		   return this.getLib_ouvrage()+" "+ this.getNumero_ordre();
		  }
		  return this.getLib_ouvrage();
		   
	   }
}
