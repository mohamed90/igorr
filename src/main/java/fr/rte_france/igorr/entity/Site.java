package fr.rte_france.igorr.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Entité définissant un site
 * @author Lopez Pierre
 * @version 1.0
 */
@Entity
public class Site { //site de postes
	/* ATTRIBUTS */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id_site;
	private String lib_site;
	//bi-directional many-to-one association to Ce 	
	@ManyToOne(fetch = FetchType.EAGER) 	
	@JoinColumn(name = "ID_GDP")
	private Gdp gdp;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy ="site")
	private List<Poste> postes = new ArrayList<Poste>();
	
	/* CONSTRUCTEUR */
	public Site() {
		
	}
	public Site(String libelle /*, Gdp gdp*/) {
		//this.id = id;
		this.lib_site = libelle;
		/*this.gdp = gdp;*/
	}
	
	/* GETTER - SETTER */
	public Long getId_site() {
		return id_site;
	}
	public void setId_site(Long id) {
		this.id_site = id;
	}
	public String getLib_site() {
		return lib_site;
	}
	public void setLibelle(String libelle) {
		this.lib_site = libelle;
	}
	public Gdp getGdp() {
		return gdp;
	}
	public void setGdp(Gdp gdp) {
		this.gdp = gdp;
	}
	public List<Poste> getPostes() {
		return postes;
	}
	public void setPostes(List<Poste> postes) {
		this.postes = postes;
	}
}
