package fr.rte_france.igorr.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Entité définissant un groupement de maintenance réseau
 * @author Lopez Pierre
 * @version 1.0
 */
@Entity
public class Gmr { //gmr chef de file
	/* ATTRIBUTS */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id_gmr;
	private String lib_gmr;
	//bi-directional many-to-one association to Ce 	
	@ManyToOne(fetch = FetchType.EAGER) 	
	@JoinColumn(name = "ID_CE")
	private Ce ce;
	//bi-directional many-to-one association to Gdp 	
	@OneToMany(fetch = FetchType.EAGER,mappedBy="gmr") 	
	private List<Gdp> gdps = new ArrayList<>();
	
	/* CONSTRUCTEUR */
	public Gmr() {
		
	}
	public Gmr( String libelle, Ce ce) {
	
		this.lib_gmr = libelle;
		this.ce = ce;
	}
	
	/* GETTER - SETTER */
	public Long getId_gmr() {
		return id_gmr;
	}
	public void setId_gmr(Long id) {
		this.id_gmr = id;
	}
	public String getLib_gmr() {
		return lib_gmr;
	}
	public void setLib_gmr(String libelle) {
		this.lib_gmr = libelle;
	}
	public Ce getCe() {
		return ce;
	}
	public void setCe(Ce ce) {
		this.ce = ce;
	}
	public List<Gdp> getGdps() {
		return gdps;
	}
	public void setGdps(List<Gdp> gdps) {
		this.gdps = gdps;
	}
}
