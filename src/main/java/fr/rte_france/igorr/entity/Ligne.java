package fr.rte_france.igorr.entity;

import javax.persistence.*;

@Entity
@Table(name = "ligne")
@DiscriminatorValue(value="LIGNE")
public class Ligne  extends Ouvrages {
	
	  private static final long serialVersionUID = 1L;
	  
	
	  // private Niveau_Tension TENSION;
	
	
	public Ligne() {}
		
	
	
	 public Ligne (String LIB_OUVRAGE, Niveau_Tension TENSION,int NUMERO_ORDRE,Gdp GRP , Ce centre) 
	 {
		  this.tension = TENSION;
		  this.lib_ouvrage = LIB_OUVRAGE;
		  this.numero_ordre = NUMERO_ORDRE;
		  this.gdp_chef_de_file = GRP;
		  this.centre = centre ;
	 }
	 /* GETTER - SETTER */
		
		/*public Niveau_Tension getTension() {
			return TENSION;
		}
		public void setTension(Niveau_Tension tension) {
			this.TENSION = tension;
		}*/
	   public String getNameOuvrage(){
			 if (this.numero_ordre!=0){
				 
				 return getTension().getVolt()+" "+ getLib_ouvrage()+" "+getNumero_ordre();
				 
				 
			 }
			 return getTension().getVolt()+" "+ getLib_ouvrage();
	          
			 }
	  
	   public  String getOuvrageWithNum(){
		   if (this.numero_ordre!=0){
			   
			   return this.getLib_ouvrage()+" "+ this.getNumero_ordre();
		   
		
		   }
		     return this.getLib_ouvrage();
	   }
		/*public int getNumero() {
			return NUMERO_ORDRE;
		}
		public void setNombre(int NOMBRE) {
			this.NUMERO_ORDRE = NOMBRE;
		}*/
	
	
}
