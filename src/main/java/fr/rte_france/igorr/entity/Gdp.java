package fr.rte_france.igorr.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Entité définissant le groupe de poste
 * @author Lopez Pierre
 * @version 1.0
 */
@Entity
public class Gdp { //chef de file gdp..ouvrages
	/* ATTRIBUTS */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id_gdp;
	private String lib_gdp;
	@ManyToOne(fetch = FetchType.EAGER) 	
	@JoinColumn(name = "ID_GMR")
	private Gmr gmr;		
	//bi-directional many-to-one association to Site 	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="gdp") 	
	private List<Site> sites = new ArrayList<Site>();
	
	/* CONTRUCTEURS */
	public Gdp() {
		
	}
	/**
	 * @param libelle
	 * @param gmr
	 */
	public Gdp( String libelle, Gmr gmr) {
		/*this.id_gdp = id;*/
		this.lib_gdp = libelle;
		this.gmr = gmr;
	}
	
	/* GETTER - SETTER */
	public Long getId_gdp() {
		return id_gdp;
	}
	public void setId_gdp(Long id) {
		this.id_gdp = id;
	}
	public String getLib_gdp() {
		return lib_gdp;
	}
	public void setLib_gdp(String libelle) {
		this.lib_gdp = libelle;
	}
	/**
	 * @return un objet gmr
	 */
	public Gmr getGmr() {
		return gmr;
	}
	public void setGmr(Gmr gmr) {
		this.gmr = gmr;
	}
	/**
	 * @return une liste de site gerés par Gdp
	 */
	public List<Site> getSites() {
		return sites;
	}
	/**
	 * @param sites
	 */
	public void setSites(List<Site> sites) {
		this.sites = sites;
	}
}
