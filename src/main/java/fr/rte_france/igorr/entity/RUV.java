package fr.rte_france.igorr.entity;

import java.util.ArrayList;
//import java.io.Serializable;
//import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
//import java.util.List;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: RUV
 *
 */
@Entity

public class RUV {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/*@OneToOne
	@JoinColumn(name="ID_GROUP1")*/
	private String source;
	/*@OneToOne
	@JoinColumn(name="ID_GROUP2")*/
	private String target;
	@ManyToMany
	 @JoinTable(
		        name = "RUV_Ouvrages", 
		        joinColumns = { @JoinColumn(name = "RVU_id") }, 
		        inverseJoinColumns = { @JoinColumn(name = "Ouvrages_id") }
		    )
	private Set <Ouvrages> ouvrages = new HashSet <Ouvrages>(0);
	@ManyToMany
	private List<GroupeProduction> group = new ArrayList<GroupeProduction>(0);
	
	public RUV(String source,String target) {
		this.source = source;
		this.target = target;
	}
	public RUV (){
		
	}
	//helpers
	public void addOuvrage(Ouvrages ouvrage){
		ouvrages.add(ouvrage); ouvrage.getRuv().add(this);
	}
	public String getName(){
		//return getTarget();
		return getSource()+"-"+getTarget();
	}
	public Set<Ouvrages> getOuvrages(){
		return this.ouvrages;
	}
	public void setOuvrages(Set<Ouvrages> ouvrages){
		this.ouvrages = ouvrages;
	}
	public List<GroupeProduction> getGroup(){
		return group;
	}
	public void setGroup(List<GroupeProduction> group){
		this.group = group;
	}
	 /* GETTER - SETTER */
	public Long getId() {
		return id;
	}
	public void setGroup1(String source) {
		this.source= source;
	}
	public void setgroup2(String target) {
		this.target = target;
	}
	public String getSource(){
	
	    return source;	
	}
	public String getTarget(){
		
	    return target;	
	}
	}

