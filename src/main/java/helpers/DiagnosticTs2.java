package helpers;

public class DiagnosticTs2 {
	private String nameOuvrage;
	private String tension;
	private String disponibilite;
	
	
	public DiagnosticTs2(){}
	
    public String getNameOuvrage(){
    	return nameOuvrage;
    }
    
    public void setNameOuvrage(String name){
    	this.nameOuvrage=name;
    }
    public String getTension(){
    	return tension;
    }
    public void setTension(String tension){
    	this.tension= tension;
    }
    public String getDisponibilite(){
    	return disponibilite;
    }
    public void setDisponibilite(String disponibilite){
    	this.disponibilite= disponibilite;
    }
}
