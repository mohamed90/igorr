package helpers;


public class DiagnotisqueOuvrage {
	

	
		private Long id_ouvrage;
		private String centre_exploitation;
		private String Gmr_chef_de_file;
		private String Gdp_chef_de_file;
		private String Scenario;
		private String Ouvrage;
		private String Tension;
		private String Type;
		private int numero_ordre;
	/*	private String disponibilité;
		private String commentaire;
		//private Ouvrages ouvrage;*/
		
		public DiagnotisqueOuvrage(){
		}
		public DiagnotisqueOuvrage(String scenario,String centre_exploitation,String gmr,String gdp,String ouvrage,String tension,String type,String dispo, String commentaire) {
			this.centre_exploitation= centre_exploitation;
			this.Scenario= scenario;
			this.Gmr_chef_de_file= gmr;
			this.Gdp_chef_de_file = gdp;
			//this.disponibilité = dispo;
			this.Ouvrage= ouvrage;
			this.Tension= tension;
			this.Type=type;
			//this.commentaire = commentaire;
			
		}
		public Long getId() {
			return id_ouvrage;
		}
		public void setId(Long id) {
			this.id_ouvrage = id;
		}
		public String getCentre_exploitation() {
			return centre_exploitation;
		}
		public int getNumero_ordre() {
			return numero_ordre;
		}
        public void setNumero_ordre(int num){
        	this.numero_ordre = num;
        }
		public void setCentre_exploitation(String centre_exploitation) {
			this.centre_exploitation = centre_exploitation;
		}

		public String getGmr() {//MEME NOM DE LATTRIBUT ATTENTION
			return Gmr_chef_de_file;
		}

		public void setGmr(String gmr) {
			this.Gmr_chef_de_file = gmr;
		}

		public String getGdp() {
			return Gdp_chef_de_file;
		}

		public void setGdp(String gdp) {
			this.Gdp_chef_de_file = gdp;
		}

		public String getOuvrage() {
			return Ouvrage;
		}

		public void setOuvrage(String ouvrage) {
			this.Ouvrage = ouvrage;
		}
	/*	public Ouvrages getObjuvrage() {
			return ouvrage;
		}

		public void setObjuvrage(Ouvrages ouvrage) {
			this.ouvrage = ouvrage;
		}*/
		
		public String getType_ouvrage() {
			return Type;
		}

		public void setType_ouvrage(String type_ouvrage) {
			this.Type = type_ouvrage;
		}

	/*	public String getDisponibilite() {
			return disponibilité;
		}

		public void setDisponibilite(String disponibilite) {
			this.disponibilité = disponibilite;
		}*/
		public String getScenario() {
			return Scenario;
		}

		public void setScenario(String ruv) {
			this.Scenario = ruv;
		}
	/*	public String getCommentaire() {
			return commentaire;
		}

		public void setCommentaire(String commentaire) {
			this.commentaire = commentaire;
		}*/

		

		public String getTension() {
			return Tension;
		}

		public void setTension(String tension) {
			this.Tension = tension;
		}

	   
	}


