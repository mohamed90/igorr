package helpers;
import java.util.List;	
import fr.rte_france.igorr.entity.DiagnosticReseau;
public class Ts2 {

		private String nameTs2;
		private String nameRuv;
		private int nbreTs2;
		private List<String> listDiaReseau;

		public Ts2(){}
		
		public Ts2(String nameTs2, String nameRuv,List<String> listDiaReseau,int nbreTs2){
			this.nameTs2=nameTs2;
			this.nameRuv= nameRuv;
			this.listDiaReseau=listDiaReseau;
			this.nbreTs2= nbreTs2;
		}
		
		
		public String getNameTs2(){
			return nameTs2;
		}
		
		public void setNameTs2(String name){
			this.nameTs2= name;
		}
		public String getNameRuv(){
			return nameRuv;
		}
		
		public void setNameRuv(String name){
			this.nameRuv= name;
		}
		public List<String> getListDiaReseau(){
			return listDiaReseau;
		}
		
		public void setListDiaReseau( List<String> name){
			this.listDiaReseau= name;
		}
		public int getNbreTs2(){
			return nbreTs2;
		}
	}



