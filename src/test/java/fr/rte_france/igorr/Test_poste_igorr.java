package fr.rte_france.igorr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.rte_france.igorr.dao.GdpDao;
import fr.rte_france.igorr.dao.CeDao;
import fr.rte_france.igorr.dao.DiagnosticReseauDao;
import fr.rte_france.igorr.dao.DiagnostiqueProducteurDao;
import fr.rte_france.igorr.dao.GmrDao;
import fr.rte_france.igorr.dao.GprodDao;
//import fr.rte_france.igorr.dao.LigneDao;
import fr.rte_france.igorr.dao.OuvragesDao;
import fr.rte_france.igorr.dao.PosteDao;
import fr.rte_france.igorr.dao.RuvDao;
import fr.rte_france.igorr.dao.SiteDao;
import fr.rte_france.igorr.entity.Niveau_Tension;

import fr.rte_france.igorr.entity.Ouvrages;
import fr.rte_france.igorr.entity.AT_Self;
import fr.rte_france.igorr.entity.AutoTransformateur;

import fr.rte_france.igorr.entity.Ce;
import fr.rte_france.igorr.entity.DiagnosticReseau;
import fr.rte_france.igorr.entity.DiagnostiqueProducteur;
import fr.rte_france.igorr.entity.Etat;
import fr.rte_france.igorr.entity.EtatProducteur;
import fr.rte_france.igorr.entity.Gdp;
import fr.rte_france.igorr.entity.Gmr;
//import fr.rte_france.igorr.dao.ProducteurDao;
import fr.rte_france.igorr.entity.Poste_Igorr;
import fr.rte_france.igorr.entity.Producteur;
import fr.rte_france.igorr.entity.ProducteurType;
import fr.rte_france.igorr.entity.RUV;
import fr.rte_france.igorr.entity.Self;
import fr.rte_france.igorr.entity.GroupeProduction;
import fr.rte_france.igorr.entity.Ligne;
import fr.rte_france.igorr.entity.Site;
import fr.rte_france.igorr.service.GererPatrimoine;

public class Test_poste_igorr {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConfigurableApplicationContext spring = new ClassPathXmlApplicationContext("spring.xml");
		PosteDao Dao = spring.getBean(PosteDao.class);
		SiteDao SiteDao = spring.getBean(SiteDao.class);
        GprodDao gproDao = spring.getBean(GprodDao.class);
       // LigneDao litDao = spring.getBean(LigneDao.class);
        CeDao ceDao = spring.getBean(CeDao.class);
        GmrDao gmrDao = spring.getBean(GmrDao.class);
        GdpDao gdpDao = spring.getBean(GdpDao.class);
        OuvragesDao ouvragesDao = spring.getBean(OuvragesDao.class);
        RuvDao ruvDao = spring.getBean(RuvDao.class);
        DiagnostiqueProducteurDao diaProd = spring.getBean(DiagnostiqueProducteurDao.class );
        DiagnosticReseauDao diaReseau = spring.getBean( DiagnosticReseauDao.class);
        //objet Ce
        List <Ce> listCe = new ArrayList<>();
    	Ce c1= new Ce("Nantes");
    	listCe.add(c1);
    	//c1.setGmrs((g7,g8,g9));
    	Ce c2= new Ce("Toulouse");
    	listCe.add(c2);
    	Ce c3= new Ce("Saint Quentin");
    	listCe.add(c3);
        Ce c4= new Ce("Lyon");
    	listCe.add(c4);
    	Ce c5= new Ce("Nancy");
    	listCe.add(c5);
    	Ce c6= new Ce("Marseille");
    	listCe.add(c6);
    	Ce c7= new Ce("Lille");
    	listCe.add(c7);
    	
    	for (Ce ce : listCe) {
    		ceDao.save(ce);
    	}
    	
    	
    	
    	//objet Gmr
    	// Gmr( String libelle, Ce ce)
    	List<Gmr> listGmr = new ArrayList<>();
    	Gmr g1= new Gmr("GMR Champagne Ardennes",c7);
    	listGmr.add(g1);
    	Gmr g2= new Gmr("GMR Flandres Hainaut",c7);
    	listGmr.add(g2);
    	Gmr g3= new Gmr("GMR Artois",c7);
    	listGmr.add(g3);
    	Gmr g4= new Gmr("GMR Bourgogne",c5);
    	listGmr.add(g4);
    	Gmr g5= new Gmr("GMR Lorraine",c5);
    	listGmr.add(g5);
    	Gmr g6= new Gmr("GMR Alsace",c5);
    	listGmr.add(g6);
    	Gmr g7= new Gmr("GMR BRETAGNE",c1);
    	listGmr.add(g7);
    	Gmr g8= new Gmr("GMR ATLANTIQUE",c1);
    	listGmr.add(g8);
    	Gmr g9= new Gmr("GMR POITOU-CHARENTES",c1);
    	listGmr.add(g9);
    	Gmr g10= new Gmr("GMR Basse Seine",c3);
    	listGmr.add(g10);
    	Gmr g11= new Gmr("GMR Nord-Ouest",c3);
    	listGmr.add(g11);
    	Gmr g12= new Gmr("GMR Normandie",c3);
    	listGmr.add(g12);
    	Gmr g13= new Gmr("GMR Est",c3);
    	listGmr.add(g13);
    	Gmr g14= new Gmr("GMR Sud-Ouest",c3);
    	listGmr.add(g14);
    	Gmr g15= new Gmr("GMR Auvergne",c4);
    	listGmr.add(g15);
    	Gmr g16= new Gmr("GMR Cote d'Azur",c6);
    	listGmr.add(g16);
    	Gmr g17= new Gmr("GMR Massif Central Ouest",c2);
    	listGmr.add(g17);
    	Gmr g18= new Gmr("GMR Champagne Morvan",c5);
    	listGmr.add(g18);
    	Gmr g19= new Gmr("GMR Gascogne",c2);
    	listGmr.add(g19);
    	Gmr g20= new Gmr("GMR Lyonnais",c4);
    	listGmr.add(g20);
    	Gmr g21= new Gmr("GMR Savoie",c4);
    	listGmr.add(g21);
    	Gmr g22= new Gmr("GMR Cevennes",c6);
    	listGmr.add(g22);
    	Gmr g23= new Gmr("GMR Bearn",c2);
    	listGmr.add(g23);
    	Gmr g24= new Gmr("GMR Languedoc-Roussillon",c2);
    	listGmr.add(g24);
    	Gmr g25= new Gmr("GMR Pyrenees",c2);
    	listGmr.add(g25);
    	Gmr g26= new Gmr("GMR ANJOU",c1);
    	listGmr.add(g26);
    	Gmr g27= new Gmr("GMR Sologne",c1);
    	listGmr.add(g27);
    	Gmr g28= new Gmr("GMR MCO",c2);
    	listGmr.add(g28);
    	Gmr g29= new Gmr("CE Toulouse",c2);
    	listGmr.add(g29);
    	Gmr g30= new Gmr("CE Saint Quentin",c3);
    	listGmr.add(g30);
    	for (Gmr ce : listGmr) {
    		gmrDao.save(ce);
    	}
    	//coorespondance entre ce et gmr
    	List<Gmr> gmrC1 = Arrays.asList(g7,g8,g9,g26);
        c1.setGmrs(gmrC1);
        List<Gmr> gmrC2 = Arrays.asList(g17,g19,g23,g24,g25);
        c2.setGmrs(gmrC2);
        List<Gmr> gmrC3 = Arrays.asList(g10,g13,g11,g12,g14);
        c2.setGmrs(gmrC3);
        List<Gmr> gmrC4 = Arrays.asList(g20,g21,g15);
        c2.setGmrs(gmrC4);
        List<Gmr> gmrC5 = Arrays.asList(g4,g5,g6,g18);
        c2.setGmrs(gmrC5);
        List<Gmr> gmrC6 = Arrays.asList(g16,g22);
        c2.setGmrs(gmrC6);
        List<Gmr> gmrC7 = Arrays.asList(g1,g2,g3);
        c2.setGmrs(gmrC7);
    	//objet Gdp
    	//public Gdp( String libelle, Gmr gmr)
    	List<Gdp> listGdp = new ArrayList<>();
    	Gdp gd20= new Gdp("Marmagne",g27);
    	listGdp.add(gd20);
    	Gdp gd21= new Gdp("Belle-epine/Tregueux",g7);
    	listGdp.add(gd21);
    	Gdp gd22= new Gdp("Fleac/Marmagne",g9);
    	listGdp.add(gd22);
    	/*Gdp gd23= new Gdp("Belle-epine/Tregueux",g18);
    	listGdp.add(gd23);*/
    	Gdp gd24= new Gdp("Niort/Fleac",g9);
    	listGdp.add(gd24);
    	Gdp gd25= new Gdp("Niort/Distré",g9);
    	listGdp.add(gd25);
    	Gdp gd26= new Gdp("Tabarderie/Marmagne",g27);
    	listGdp.add(gd26);
    	Gdp gd27= new Gdp("Chaingy/Marmagne",g27);
    	listGdp.add(gd27);
    	Gdp gd28= new Gdp("Tabarderie/Chaingy",g27);
    	listGdp.add(gd28);
    	Gdp gd29= new Gdp("Chaingy/Larcay",g27);
    	listGdp.add(gd29);
    	Gdp gd30= new Gdp("CE Toulouse",g9);//gmr PCH
    	listGdp.add(gd30);
    	Gdp gd36= new Gdp("CE Toulouse",g28);//gmr mco
    	listGdp.add(gd36);
    	Gdp gd37= new Gdp("CE Toulouse",g27);//gmr sologne
    	listGdp.add(gd37);
    	Gdp gd38= new Gdp("CE Toulouse",g29);
    	listGdp.add(gd38);
    	Gdp gd39= new Gdp("CE Saint Quentin",g30);
    	listGdp.add(gd39);
    	Gdp gd35= new Gdp("CE Nancy",g18);
    	listGdp.add(gd35);
    	Gdp gd31= new Gdp("CE Lyon",g15);//gmr auv
    	listGdp.add(gd31);
    	Gdp gd32= new Gdp("CE SQY",g13);//gmr est
    	listGdp.add(gd32);
    	Gdp gd33= new Gdp("CE SQY",g14);//sud ouest
    	listGdp.add(gd33);
    	Gdp gd34= new Gdp("CE SQY",g27);//sologne
    	listGdp.add(gd34);
    	Gdp gd1= new Gdp("Garchizy",g18);
    	listGdp.add(gd1);
    	Gdp gd2= new Gdp("Poteau-rouge",g4);
    	listGdp.add(gd2);
    	Gdp gd3= new Gdp("Tregueux",g7);
    	listGdp.add(gd3);
    	Gdp gd4= new Gdp("Belle-epine",g7);
    	listGdp.add(gd4);
    	Gdp gd5= new Gdp("Cordemais",g8);
    	listGdp.add(gd5);
    	Gdp gd6= new Gdp("Sirmiere",g8);
    	listGdp.add(gd6);
    	Gdp gd7= new Gdp("Fleac",g9);
    	listGdp.add(gd7);
    	Gdp gd8= new Gdp("Niort",g9);
    	listGdp.add(gd8);
    	Gdp gd9= new Gdp("Agneaux",g12);
    	listGdp.add(gd9);
    	Gdp gd10= new Gdp("Arrighi",g13);
    	listGdp.add(gd10);
    	Gdp gd11= new Gdp("Essonne",g14);
    	listGdp.add(gd11);
    	Gdp gd12= new Gdp("Chesnoy",g13);
    	listGdp.add(gd12);
    	Gdp gd13= new Gdp("Yvelines",g14);
    	listGdp.add(gd13);
    	Gdp gd14= new Gdp("Bourbonnais",g15);
    	listGdp.add(gd14);
    	Gdp gd15= new Gdp("Limousin",g15);
    	listGdp.add(gd15);
    	Gdp gd16= new Gdp("Chaingy",g27);
    	listGdp.add(gd16);
    	Gdp gd17= new Gdp("Tabarderie",g27);
    	listGdp.add(gd17);
    	Gdp gd18= new Gdp("Distre",g26);
    	listGdp.add(gd18);
    	Gdp gd19= new Gdp("Larcay",g26);
    	listGdp.add(gd19);
    	
    	for (Gdp ce : listGdp) {
    		gdpDao.save(ce);
    	}
    	  /*ouvrages*/
 	   /*ligne*/
 	     //public Ligne (String LIB_OUVRAGE, Niveau_Tension TENSION,int NUMERO_ORDRE) 
 	    Ligne l1 = new Ligne ("Avoine-Distré",Niveau_Tension.SIX,0,gd18,c1);
 	    Ligne l2 = new Ligne ("Distré-Mondion",Niveau_Tension.SIX,0,gd18,c1);
 	    Ligne l3 = new Ligne ("mondion-Orangerie",Niveau_Tension.SIX,0,gd18,c1);
 	    Ligne l4 = new Ligne ("Distré-picocherie",Niveau_Tension.SEPT,0,gd18,c1);
 	    Ligne l5 = new Ligne ("Chaceaux-Distré",Niveau_Tension.SEPT,0,gd18,c1);
 	    Ligne l6 = new Ligne ("Avoine-Distré",Niveau_Tension.SEPT,1,gd18,c1);
 	    Ligne l7 = new Ligne ("Avoine-Distré",Niveau_Tension.SEPT,2,gd18,c1);
 	    Ligne l8 = new Ligne ("Bayet-Gauglin",Niveau_Tension.SEPT,0,gd31,c4);
 	    Ligne l9 = new Ligne ("Bayet-Marmagne",Niveau_Tension.SEPT,1,gd31,c4);
 	    Ligne l10 = new Ligne ("Bayet-Marmagne",Niveau_Tension.SEPT,2,gd31,c4);
 	    Ligne l11 = new Ligne ("Domloup-Launay",Niveau_Tension.SEPT,1,gd4,c1);
 	    Ligne l12 = new Ligne ("Domloup-Launay",Niveau_Tension.SEPT,2,gd4,c1);
 	    Ligne l13 = new Ligne ("Brennelis-Rospez-Plaine Haute",Niveau_Tension.SIX,0,gd3,c1);
 	    Ligne l14 = new Ligne ("Plaine Haute-Tregueux",Niveau_Tension.SIX,1,gd3,c1);
 	    Ligne l15 = new Ligne ("Plaine Haute-Tregueux",Niveau_Tension.SIX,2,gd3,c1);
 	    Ligne l16 = new Ligne ("Doberie-Tregueux",Niveau_Tension.SIX,0,gd3,c1);
 	    Ligne l17 = new Ligne ("Doberie-Rance",Niveau_Tension.SIX,0,gd21,c1);
 	    Ligne l18 = new Ligne ("Launay-Rance",Niveau_Tension.SIX,0,gd4,c1);
 	    Ligne l19 = new Ligne ("Gauglin-st Eloi",Niveau_Tension.SEPT,0,gd35,c5);
 	    Ligne l20 = new Ligne ("Chesnoy-Tabarderie",Niveau_Tension.SEPT,1,gd32,c3);
 	    Ligne l21 = new Ligne ("Chesnoy-Tabarderie",Niveau_Tension.SEPT,2,gd32,c3);
 	    Ligne l22 = new Ligne ("Chesnoy-Tabarderie",Niveau_Tension.SEPT,3,gd32,c3);
 	    Ligne l23 = new Ligne ("Eguzon-Rueyre",Niveau_Tension.SEPT,0,gd36,c2);
 	    Ligne l24 = new Ligne ("Eguzon-Plaud",Niveau_Tension.SEPT,0,gd36,c2);
 	    Ligne l25 = new Ligne ("Braud-Granzay-Preguillac",Niveau_Tension.SEPT,1,gd30,c1);
 	    Ligne l26 = new Ligne ("Braud-Granzay-Preguillac",Niveau_Tension.SEPT,2,gd30,c1);
 	    Ligne l27 = new Ligne ("Eguzon-Valdivienne",Niveau_Tension.SEPT,1,gd22,c1);
 	    Ligne l28 = new Ligne ("Eguzon-Valdivienne",Niveau_Tension.SEPT,2,gd22,c1);
 	    //Ligne l29 = new Ligne ("Eguzon-Valdivienne",Niveau_Tension.SEPT,0,gd22);
 	    Ligne l30 = new Ligne ("Eguzon-Orangerie",Niveau_Tension.SIX,0,gd22,c1);
 	    Ligne l31 = new Ligne ("Distre-Jumeaux-Granzay",Niveau_Tension.SEPT,1,gd25,c1);
 	    Ligne l32 = new Ligne ("Distre-Jumeaux-Granzay",Niveau_Tension.SEPT,2,gd25,c1);
 	    Ligne l33 = new Ligne ("Granzay-Rom",Niveau_Tension.SEPT,1,gd7,c1);
 	    Ligne l34 = new Ligne ("Rom-Valdienne",Niveau_Tension.SEPT,1,gd24,c1);
 	    Ligne l35 = new Ligne ("Granzay-Valdivienne",Niveau_Tension.SEPT,2,gd24,c1);
 	    Ligne l36 = new Ligne ("Dambron-Yvelines Ouest",Niveau_Tension.SEPT,0,gd33,c3);
 	    Ligne l37 = new Ligne ("Dambron-villejust",Niveau_Tension.SEPT,2,gd33,c3);
 	    Ligne l38 = new Ligne ("Gatinais-Gauglin",Niveau_Tension.SEPT,1,gd17,c1);
 	    Ligne l39 = new Ligne ("Gatinais-Gauglin",Niveau_Tension.SEPT,2,gd17,c1);
 	    Ligne l40 = new Ligne ("Gatinais-Tabarderie",Niveau_Tension.SEPT,1,gd17,c1);
 	    Ligne l41 = new Ligne ("Gatinais-Tabarderie",Niveau_Tension.SEPT,2,gd17,c1);
 	    Ligne l42 = new Ligne ("Cirolliers-Gatinais",Niveau_Tension.SEPT,1,gd34,c1);
 	    Ligne l43 = new Ligne ("Cirolliers-Gatinais",Niveau_Tension.SEPT,2,gd34,c1);
 	    Ligne l44 = new Ligne ("Marmagne-Tabarderie",Niveau_Tension.SEPT,1,gd26,c1);
 	    Ligne l45 = new Ligne ("Marmagne-Tabarderie",Niveau_Tension.SEPT,1,gd26,c1);
 	    Ligne l46 = new Ligne ("Breuil-Marmagne",Niveau_Tension.SEPT,0,gd37,c1);
 	    Ligne l47 = new Ligne ("Eguzon-Verger",Niveau_Tension.SEPT,0,gd27,c1);
 	    Ligne l48 = new Ligne ("Chaingy-terres Noires-St Laurent",Niveau_Tension.SIX,0,gd27,c1);
 	    Ligne l49 = new Ligne ("Eguzon-Terres Noires",Niveau_Tension.SIX,0,gd20,c1);
 	    Ligne l50 = new Ligne ("Dambron-Gatinais",Niveau_Tension.SEPT,1,gd28,c1);
 	    Ligne l51 = new Ligne ("Dambron-Gatinais",Niveau_Tension.SEPT,2,gd28,c1);
 	    Ligne l52 = new Ligne ("Dambron-Verger",Niveau_Tension.SEPT,1,gd16,c1);
 	    Ligne l53 = new Ligne ("Dambron-Verger",Niveau_Tension.SEPT,2,gd16,c1);
 	    Ligne l54 = new Ligne ("Picocherie-Villerbon-verger",Niveau_Tension.SEPT,0,gd29,c1);
 	    Ligne l55 = new Ligne ("Chanceaux-Villerbon-verger",Niveau_Tension.SEPT,0,gd29,c1);
 	    //Ligne l56 = new Ligne ("Picocherie-Villerbon-verger",Niveau_Tension.SEPT,2);
 	    /*ouvrages toulouse*/
 	    Ligne l57 = new Ligne ("Eguzon-Mole",Niveau_Tension.SIX,0,gd38,c2);
 	    Ligne l58 = new Ligne ("Eguzon-Mole et mole-St Feye",Niveau_Tension.SIX,0,gd38,c2);
 	    Ligne l59 = new Ligne ("Breuil-Mole",Niveau_Tension.SIX,1,gd38,c2);
 	    Ligne l60 = new Ligne ("Breuil-Mole",Niveau_Tension.SIX,2,gd38,c2);
 	    Ligne l61 = new Ligne ("montezic-Rueyre",Niveau_Tension.SEPT,0,gd38,c2);
 	    Ligne l62 = new Ligne ("Bort-Mole",Niveau_Tension.SIX,1,gd38,c2);
 	    Ligne l63 = new Ligne ("Bort-Mole",Niveau_Tension.SIX,2,gd38,c2);
 	    Ligne l64 = new Ligne ("Brommat-Rueyres",Niveau_Tension.SEPT,0,gd38,c2);
 	    Ligne l65 = new Ligne ("Breuil-Chastang",Niveau_Tension.SIX,3,gd38,c2);
 	    Ligne l66 = new Ligne ("Aigle 6-Breuil",Niveau_Tension.SIX,0,gd38,c2);
 	     Ligne l81 = new Ligne ("Breuil-Rueyre",Niveau_Tension.SEPT,0,gd38,c2);
 	    /*ouvrages st quentin*/
 	   // Ligne l67 = new Ligne ("Dambron-villejust",Niveau_Tension.SEPT,2);
 	    Ligne l67 = new Ligne ("Cirolliers-Villejust",Niveau_Tension.SEPT,1,gd39,c3);
 	    Ligne l68 = new Ligne ("Cirolliers-Villejust",Niveau_Tension.SEPT,2,gd39,c3);
 	    Ligne l69 = new Ligne ("Cirolliers-Villejust",Niveau_Tension.SEPT,3,gd39,c3);
 	   // Ligne l70 = new Ligne ("Cirolliers-Villejust",Niveau_Tension.SEPT,1);
 	    Ligne l70 = new Ligne ("Chevilly-Villejust",Niveau_Tension.SIX,1,gd39,c3);
 	    Ligne l71 = new Ligne ("Chevilly-Villejust",Niveau_Tension.SIX,2,gd39,c3);
 	    Ligne l72 = new Ligne ("Chevilly-Villejust",Niveau_Tension.SIX,3,gd39,c3);
 	    Ligne l73 = new Ligne ("Arrighy-Chevilly",Niveau_Tension.SIX,1,gd39,c3);
 	    Ligne l74 = new Ligne ("arrighy-Villeneuve St George-Chevilly",Niveau_Tension.SIX,0,gd39,c3);
 	    Ligne l75 = new Ligne ("Launay-Taute",Niveau_Tension.SIX,1,gd39,c3);
 	    Ligne l76 = new Ligne ("Launay-Taute",Niveau_Tension.SEPT,2,gd39,c3);
 	    Ligne l77 = new Ligne ("Taute-Menuel",Niveau_Tension.SEPT,1,gd39,c3);
 	    Ligne l78 = new Ligne ("Taute-Menuel",Niveau_Tension.SEPT,2,gd39,c3);
 	    Ligne l79 = new Ligne ("Taute-Menuel",Niveau_Tension.SEPT,3,gd39,c3);
 	    Ligne l80 = new Ligne ("Taute-Menuel",Niveau_Tension.SEPT,4,gd39,c3);
 	    /* ouvrage self*/
 	    // public Self (String LIB_OUVRAGE,int NUMERO_ORDRE) 
 	    Self s1 = new Self("Arrighi",Niveau_Tension.UN,0,gd39,c3);
 	    Self s2 = new Self("Menuel",Niveau_Tension.UN,1,gd39,c3);
 	    Self s3 = new Self("Menuel",Niveau_Tension.UN,3,gd39,c3);
 	    /*AT+SELFR*/
 	    // public AT_Self (String LIB_OUVRAGE, Niveau_Tension TENSION,int NUMERO_ORDRE) 
 	    Ouvrages ats1 = new AT_Self("Villejust AT764 ou 762 ou 761",Niveau_Tension.SEPTSIX,5,gd39,c3);
 	    AT_Self ats2 = new AT_Self("Chevilly AT764 ou 762 ou 761",Niveau_Tension.SEPTSIX,5,gd39,c3);
 	    //AT_Self ats3 = new AT_Self("Launay",Niveau_Tension.SEPTSIX,3);
 	    AT_Self ats4 = new AT_Self("Breuil",Niveau_Tension.SEPTSIX,2,gd38,c2);
 	    AT_Self ats5 = new AT_Self("Eguzon",Niveau_Tension.SEPTSIX,1,gd20,c3);
 	    AT_Self ats6 = new AT_Self("Eguzon",Niveau_Tension.SEPTSIX,2,gd20,c3);
 	    AT_Self ats7 = new AT_Self("Marmagne",Niveau_Tension.SEPTSIX,1,gd20,c3);
 	   AT_Self ats3 = new AT_Self("Rueyres 761 ou 762",Niveau_Tension.SEPTSIX,1,gd38,c2);
 	   /*jeu de barre*/
 	  
 	    
 	    /*AT*/
 	    // public AutoTransformateur (String LIB_OUVRAGE, Niveau_Tension TENSION,int NUMERO_ORDRE) 
 	    AutoTransformateur at1 = new AutoTransformateur("Avoine",Niveau_Tension.SEPTSIX,1,gd18,c1);
 	    AutoTransformateur at2 = new AutoTransformateur("Launay",Niveau_Tension.SEPTSIX,3,gd39,c3);
 	   //DiagnostiqueProducteur(String centre_exploitation,String groupe, String etat, String ts, String commentaire)
 	 
    	//corespondnace gmr et gdp
    	List<Gdp> gdp1 = Arrays.asList(gd14,gd15,gd16);
        g15.setGdps(gdp1);
        List<Gdp> gdp2 = Arrays.asList(gd11,gd13);
        g14.setGdps(gdp2);
        List<Gdp> gdp3 = Arrays.asList(gd10,gd12);
        g13.setGdps(gdp3);
        List<Gdp> gdp4 = Arrays.asList(gd9);
        g12.setGdps(gdp4);
        List<Gdp> gdp5 = Arrays.asList(gd7,gd8);
        g9.setGdps(gdp5);
        List<Gdp> gdp6 = Arrays.asList(gd6,gd5);
        g8.setGdps(gdp6);
        List<Gdp> gdp7 = Arrays.asList(gd4,gd2,gd3);
        g4.setGdps(gdp7);
        List<Gdp> gdp8 = Arrays.asList(gd17);
        g19.setGdps(gdp8);
        List<Gdp> gdp9 = Arrays.asList(gd1);
        g18.setGdps(gdp9);
        List<Gdp> gdp10 = Arrays.asList(gd18,gd19);
        g26.setGdps(gdp10);
        //objet Site
		 Site element1 = new Site("AVOINE");
		 Site element2 = new Site("VALDIVIENNE");
		 Site element3 = new Site("TABARDERIE");
		// Site element5 = new Site("CORDEMAIS");
		 Site element6 = new Site("CAUGLAIS");
		 Site element7 = new Site("VERGER");
		 Site element8 = new Site("DISTRé");
		 Site element9 = new Site("ARRIGHY");
		 Site element10 = new Site("BREUIL");
		 Site element11= new Site("VILLEJUST");
		 Site element12 = new Site("CHEVILLY");
		 Site element13 = new Site("MOLE");
		 Site element14 = new Site ("BRENNILIS");
		 Site element15 = new Site ("CHASTANG");
		 Site element16 = new Site ("MONTEZIC");
		 Site element17 = new Site ("BROMMAT");
		 
		 //SiteDao.save(element5);
		 SiteDao.save(element6);
		 SiteDao.save(element7);
		 SiteDao.save(element1);
		 SiteDao.save(element2);
		 SiteDao.save(element3);
		 SiteDao.save(element8);
		 SiteDao.save(element9);
		 SiteDao.save(element10);
		 SiteDao.save(element11);
		 SiteDao.save(element12);
		 SiteDao.save(element13);
		 SiteDao.save(element14);
		 SiteDao.save(element15);
		 SiteDao.save(element16);
		 SiteDao.save(element17);
	//Créer objet poste_IGORR
		 //(String LIB_OUVRAGE,Site site, Niveau_Tension TENSION,int NUMERO_ORDRE,Gdp CHEF_DE_FILE,Ce centre)
		Poste_Igorr el81 =  new Poste_Igorr("MONTEZIC",element16,Niveau_Tension.SEPT,gd38,c2);
		Poste_Igorr el80 =  new Poste_Igorr("BROMMAT",element17,Niveau_Tension.SEPT,gd38,c2);
		Poste_Igorr el79 =  new Poste_Igorr("CHASTANG",element15,Niveau_Tension.SIX,gd38,c2);
		//Poste_Igorr el78 =  new Poste_Igorr("BRENNILIS",element14,Niveau_Tension.SIX,); 
		Poste_Igorr el1 = new Poste_Igorr("AVOINE ",element1,Niveau_Tension.SEPT,gd18,c1);
		Poste_Igorr el2 = new Poste_Igorr("VALDIVIENNE",element2,Niveau_Tension.SEPT,gd7,c1);
		Poste_Igorr el3 =  new Poste_Igorr("TABARDERIE ",element3,Niveau_Tension.SEPT,gd17,c1);
		//Poste_Igorr el6 =  new Poste_Igorr("cordemais 400kv",element3,"COR.P",Niveau_Tension.SEPT,332094.7892,6698627.744);
		Poste_Igorr el10 =  new Poste_Igorr("CAUGLAI",element6,Niveau_Tension.SEPT,gd17,c1);
		Poste_Igorr el11 =  new Poste_Igorr("VERGER",element7,Niveau_Tension.SEPT,gd16,c1);
		//Poste_Igorr el21 =  new Poste_Igorr("DISTRE",element8,Niveau_Tension.SIX,);
		//Poste_Igorr el22 =  new Poste_Igorr("avoine 225kv",element1,"AVOIN",Niveau_Tension.SIX,1155622.85,-25333563.23);
		//Poste_Igorr el23 =  new Poste_Igorr("distr2 400kv",element8,"DISTR",Niveau_Tension.SEPT,1155622.36,-25333563.23);//el22-el23 400 et 225
		Poste_Igorr el24 =  new Poste_Igorr("ARRIGHI",element9,Niveau_Tension.SIX,gd39,c3);
		//Poste_Igorr el25 =  new Poste_Igorr("CHEVILLY 225kv",element12,"CHEVI",Niveau_Tension.SIX,1155622.17,-25333563.23);		
		//Poste_Igorr el26 =  new Poste_Igorr("VILLEJUST 225kv",element11,"VLEJU",Niveau_Tension.SIX,1155622.35,-25333563.23);
		//Poste_Igorr el27 =  new Poste_Igorr("BREUIL 225kv",element10,"BREUI",Niveau_Tension.SIX,1155622.23,-25333563.23);
		//Poste_Igorr el28 =  new Poste_Igorr("MOLE 225kv",element13,"MOLE ",Niveau_Tension.SIX,1155622.69,-25333563.23);
		//ouvrage liaison //Ligne (int nbresTermes,Poste_Igorr source ,Poste_Igorr target,Etat etat) 
	/*	Ligne l1 = new Ligne(1,el1,el23,Etat.INCONNU);//avoin-distr
		Ligne l2 = new Ligne(2,el1,el23,Etat.INCONNU);
		Ligne l3 = new Ligne(1,el27,el28,Etat.INCONNU);//breuil-distr
		Ligne l4 = new Ligne(2,el27,el28,Etat.INCONNU);
		Ligne l5 = new Ligne(1,el25,el26,Etat.INCONNU);//chevilly-villjust
		Ligne l6 = new Ligne(2,el25,el26,Etat.INCONNU);
		Ligne l7 = new Ligne(3,el25,el26,Etat.INCONNU);
		Ligne l8 = new Ligne(1,el24,el25,Etat.INCONNU);//ARRIGHI-CHEVIL*/
		//
		 //ruv
	    //public RUV(GroupeProduction source,GroupeProduction target)
	    RUV rv1 = new RUV("DAMPIERRE","BELLEVILLE");
	    rv1.addOuvrage(l38);//soit setliste a prevoir 
	    rv1.addOuvrage(l39);
	    rv1.addOuvrage(l40);
	    rv1.addOuvrage(l41);
	    RUV rv2 = new RUV("ARRIGHI","BELLEVILLE");
	    rv2.addOuvrage(l38);
	    rv2.addOuvrage(l39);
	    rv2.addOuvrage(l42);
	    rv2.addOuvrage(l43);
	    rv2.addOuvrage(l67);
	    rv2.addOuvrage(l68);
	    rv2.addOuvrage(l69);
	    rv2.addOuvrage(l70);
	    rv2.addOuvrage(l71);
	    rv2.addOuvrage(l72);
	    rv2.addOuvrage(l73);
	    rv2.addOuvrage(l74);
	    rv2.addOuvrage(s1);
	    rv2.addOuvrage(ats1);
	    rv2.addOuvrage(ats2);
	    RUV rv3 = new RUV("BORT","CHINON");
	    rv3.addOuvrage(at1);
	    rv3.addOuvrage(l1);
	    rv3.addOuvrage(l2);
	    rv3.addOuvrage(l3);
	    rv3.addOuvrage(l30);
	    rv3.addOuvrage(l57);
	    rv3.addOuvrage(l58);
	    rv3.addOuvrage(l59);
	    rv3.addOuvrage(l60);
	    rv3.addOuvrage(l62);
	    rv3.addOuvrage(l63);
	    rv3.addOuvrage(ats4);
	    RUV rv4 = new RUV("CHASTANG","CHINON");
	    rv4.addOuvrage(at1);
	    rv4.addOuvrage(l1);
	    rv4.addOuvrage(l2);
	    rv4.addOuvrage(l3);
	    rv4.addOuvrage(l30);
	    rv4.addOuvrage(l57);
	    rv4.addOuvrage(l58);
	    rv4.addOuvrage(l59);
	    rv4.addOuvrage(l60);
	    rv4.addOuvrage(ats4);
	   // rv4.addOuvrage(l63);
	    rv4.addOuvrage(l65);
	    RUV rv5 = new RUV("MONTEZIC","CIVAUX");
	    rv5.addOuvrage(ats5);
	    rv5.addOuvrage(ats6);
	    rv5.addOuvrage(ats3);
	    rv5.addOuvrage(l27);
	    rv5.addOuvrage(l28);
	    rv5.addOuvrage(l61);
	    rv5.addOuvrage(l23);
	    RUV rv6 = new RUV("BORT","CIVAUX");
	    rv6.addOuvrage(ats5);
	    rv6.addOuvrage(ats6);
	    rv6.addOuvrage(l58);
	    rv6.addOuvrage(l27);
	    rv6.addOuvrage(l28);
	    rv6.addOuvrage(l62);
	    rv6.addOuvrage(l63);
	    rv6.addOuvrage(l57);
	    RUV rv7 = new RUV("MONTEZIC","DAMPIERRE");
	    rv7.addOuvrage(l61);
	    rv7.addOuvrage(l46);
	    rv7.addOuvrage(l81);
	    rv7.addOuvrage(l44);
	    rv7.addOuvrage(l45);
	    rv7.addOuvrage(ats7);
	    rv7.addOuvrage(ats3);
	    rv7.addOuvrage(ats4);
	    RUV rv8 = new RUV("AIGLE6","DAMPIERRE");
	    rv8.addOuvrage(l44);
	    rv8.addOuvrage(l45);
	    rv8.addOuvrage(l46);
	    rv8.addOuvrage(ats7);
	    rv8.addOuvrage(ats4);
	    rv8.addOuvrage(l66);
	    RUV rv9 = new RUV("BROMMAT","ST LAURENT");
	    rv9.addOuvrage(l23);
	    rv9.addOuvrage(l47);
	    rv9.addOuvrage(l64);
	    rv9.addOuvrage(ats5);
	    rv9.addOuvrage(ats6);
	    rv9.addOuvrage(ats3);
	    RUV rv10 = new RUV("CHASTANG","ST LAURENT");
	    rv10.addOuvrage(l48);
	    rv10.addOuvrage(l49);
	    rv10.addOuvrage(l59);
	    rv10.addOuvrage(l58);
	    rv10.addOuvrage(l57);
	    rv10.addOuvrage(l60);
	    rv10.addOuvrage(ats4);
	    rv10.addOuvrage(l65);
	    RUV rv11 = new RUV("ARRIGHI","ST LAURENT");
	    rv11.addOuvrage(l52);
	    rv11.addOuvrage(l53);
	    rv11.addOuvrage(ats1);
	    rv11.addOuvrage(l37);
	    rv11.addOuvrage(l70);
	    rv11.addOuvrage(l71);
	    rv11.addOuvrage(l72);
	    rv11.addOuvrage(l73);
	    RUV rv12 = new RUV("BRENNELIS","FLAMMAVILLE");
	    rv12.addOuvrage(at2);
	    rv12.addOuvrage(s2);
	    rv12.addOuvrage(s3);
	    rv12.addOuvrage(l75);
	    rv12.addOuvrage(l76);
	    rv12.addOuvrage(l77);
	    rv12.addOuvrage(l78);
	    rv12.addOuvrage(l79);
	    rv12.addOuvrage(l80);
	    RUV rv13 = new RUV("LIAISON BOUCLAGE","");
	    rv13.addOuvrage(l8);
	    rv13.addOuvrage(l9);
	    rv13.addOuvrage(l10);
	    rv13.addOuvrage(l11);
	    rv13.addOuvrage(l12);
	    rv13.addOuvrage(l19);
	    rv13.addOuvrage(l20);
	    rv13.addOuvrage(l21);
	    rv13.addOuvrage(l22);
	    rv13.addOuvrage(l24);
	    rv13.addOuvrage(l25);
	    rv13.addOuvrage(l26);
	    rv13.addOuvrage(l36);
	    rv13.addOuvrage(l37);
	    RUV rv14 = new RUV("BELLEVILLE","BELLEVILLE");
	    rv14.addOuvrage(el10);
	    RUV rv15 = new RUV("CHINON","CHINON");
	    rv15.addOuvrage(el1);
	    RUV rv16 = new RUV("CIVAUX","CIVAUX");
	    rv16.addOuvrage(el2);
	    RUV rv17 = new RUV("DAMPIERRE","DAMPIERRE");
	    rv17.addOuvrage(el3);
	    RUV rv18 = new RUV("ST LAURENT","ST LAURENT");
	    rv18.addOuvrage(el11);
	    /*diareseu*/
	 	  // public DiagnosticReseau(String centre_exploitation,String ouvrage,String tension,String type,String dispo, String commentaire)
	 	   DiagnosticReseau dia10= new DiagnosticReseau(rv11.getName(),l70.getcentre().getLib_ce(),l70.getOuvrageWithNum(),l70.getTension().getVolt(),"LIGNE","disponible","fjdfjfjd",l70);
	 	   DiagnosticReseau dia11= new DiagnosticReseau(rv11.getName(),l72.getcentre().getLib_ce(),l72.getOuvrageWithNum(),l72.getTension().getVolt(),"LIGNE","inconnu","fjdfjfjd",l72);
	 	   DiagnosticReseau dia12= new DiagnosticReseau(rv11.getName(),l52.getcentre().getLib_ce(),l52.getOuvrageWithNum(),l52.getTension().getVolt(),"LIGNE","inconnu","fjdfjfjd",l52);
	 	   DiagnosticReseau dia13= new DiagnosticReseau(rv9.getName(),l64.getcentre().getLib_ce(),l64.getOuvrageWithNum(),l64.getTension().getVolt(),"LIGNE","inconnu","fjdfjfjd",l64);
	 	   DiagnosticReseau dia14= new DiagnosticReseau(rv9.getName(),ats6.getcentre().getLib_ce(),ats6.getOuvrageWithNum(),ats6.getTension().getVolt(),"AT+SELF","inconnu","fjdfjfjd",ats6);
	 	  DiagnosticReseau dia15= new DiagnosticReseau(rv9.getName(),l23.getcentre().getLib_ce(),l23.getOuvrageWithNum(),l23.getTension().getVolt(),"LIGNE","inconnu","fjdfjfjd",l23);
	 	  DiagnosticReseau dia16= new DiagnosticReseau(rv18.getName(),el11.getcentre().getLib_ce(),el11.getOuvrageWithNum(),el11.getTension().getVolt(),"POSTE","inconnu","fjdfjfjd",el11);
	 	  DiagnosticReseau dia17= new DiagnosticReseau(rv10.getName(),ats4.getcentre().getLib_ce(),ats4.getOuvrageWithNum(),ats4.getTension().getVolt(),"AT+SELF","disponible","fjdfjfjd",ats4);
	 	  DiagnosticReseau dia18= new DiagnosticReseau(rv10.getName(),l57.getcentre().getLib_ce(),l57.getOuvrageWithNum(),l57.getTension().getVolt(),"LIGNE","inconnu","fjdfjfjd",l57);
	 	 DiagnosticReseau dia19= new DiagnosticReseau(rv17.getName(),el3.getcentre().getLib_ce(),el3.getOuvrageWithNum(),el3.getTension().getVolt(),"POSTE","inconnu","fjdfjfjd",el3);
	 	 DiagnosticReseau dia20= new DiagnosticReseau(rv7.getName(),l46.getcentre().getLib_ce(),l46.getOuvrageWithNum(),l46.getTension().getVolt(),"LIGNE","inconnu","fjdfjfjd",l46);
	 	 DiagnosticReseau dia21= new DiagnosticReseau(rv7.getName(),l44.getcentre().getLib_ce(),l44.getOuvrageWithNum(),l44.getTension().getVolt(),"LIGNE","inconnu","fjdfjfjd",l44);
	 	 DiagnosticReseau dia22= new DiagnosticReseau(rv7.getName(),l61.getcentre().getLib_ce(),l61.getOuvrageWithNum(),l61.getTension().getVolt(),"LIGNE","inconnu","fjdfjfjd",l61);
	 	 DiagnosticReseau dia23= new DiagnosticReseau(rv8.getName(),l66.getcentre().getLib_ce(),l66.getOuvrageWithNum(),l66.getTension().getVolt(),"LIGNE","disponible","fjdfjfjd",l66);
	 	 DiagnosticReseau dia24= new DiagnosticReseau(rv8.getName(),ats4.getcentre().getLib_ce(),ats4.getOuvrageWithNum(),ats4.getTension().getVolt(),"AT+SELF","inconnu","fjdfjfjd",ats4);
	 	 DiagnosticReseau dia25= new DiagnosticReseau(rv5.getName(),ats6.getcentre().getLib_ce(),ats6.getOuvrageWithNum(),ats6.getTension().getVolt(),"AT+SELF","inconnu","fjdfjfjd",ats6);
	 	 DiagnosticReseau dia26= new DiagnosticReseau(rv5.getName(),l61.getcentre().getLib_ce(),l61.getOuvrageWithNum(),l61.getTension().getVolt(),"LIGNE","disponible","fjdfjfjd",l61);
	 	 DiagnosticReseau dia27= new DiagnosticReseau(rv16.getName(),el2.getcentre().getLib_ce(),el2.getOuvrageWithNum(),el2.getTension().getVolt(),"POSTE","inconnu","fjdfjfjd",el2);
	 	 DiagnosticReseau dia28= new DiagnosticReseau(rv6.getName(),l63.getcentre().getLib_ce(),l63.getOuvrageWithNum(),l63.getTension().getVolt(),"LIGNE","inconnu","fjdfjfjd",l63);
	 	 DiagnosticReseau dia29= new DiagnosticReseau(rv6.getName(),l28.getcentre().getLib_ce(),l28.getOuvrageWithNum(),l28.getTension().getVolt(),"LIGNE","disponible","fjdfjfjd",l28);
	 	 //DiagnosticReseau dia30= new DiagnosticReseau(rv8.getName(),l66.getcentre().getLib_ce(),l66.getOuvrageWithNum(),l66.getTension().getVolt(),"LIGNE","disponible","fjdfjfjd",l66);
	 	 DiagnosticReseau dia31= new DiagnosticReseau(rv5.getName(),l23.getcentre().getLib_ce(),l23.getOuvrageWithNum(),l23.getTension().getVolt(),"LIGNE","inconnu","fjdfjfjd",l23);
	 	
	 	
		//l1.getAppelationRef();
		//System.out.println(l1.getRefLigne());
		//objet groupe
		//public GroupeProduction(String name ,String libelle_poste,ProducteurType type ,String region,double x, double y )
		//public GroupeProduction(String name ,String libelle_poste,int num ,ProducteurType type ,String region,double x, double y )
		GroupeProduction el12 = new GroupeProduction ("BELLEVILLE 1","BVILX",1,ProducteurType.NUCLEAIRE,"Nantes");
		el12.addRuv(rv1);
		el12.addRuv(rv2);
		el12.addRuv(rv14);
		GroupeProduction el13 = new GroupeProduction ("BELLEVILLE 2","BVIL7",2,ProducteurType.NUCLEAIRE,"Nantes");
		el13.addRuv(rv1);
		el13.addRuv(rv2);
		el13.addRuv(rv14);
		
		GroupeProduction el14 = new GroupeProduction ("DAMPIERRE 2","D.BUR",2,ProducteurType.NUCLEAIRE,"Nantes");
		el14.addRuv(rv17);
		el14.addRuv(rv8);
		el14.addRuv(rv7);
		
		GroupeProduction el15 = new GroupeProduction ("DAMPIERRE 3","D.BUR",3,ProducteurType.NUCLEAIRE,"Nantes");
		el15.addRuv(rv17);
		el15.addRuv(rv8);
		el15.addRuv(rv7);
		GroupeProduction el16 = new GroupeProduction ("DAMPIERRE 4","D.BUR",4,ProducteurType.NUCLEAIRE,"Nantes");
		el16.addRuv(rv17);
		el16.addRuv(rv8);
		el16.addRuv(rv7);
		GroupeProduction el17 = new GroupeProduction ("DAMPIERRE 1","D.BUR",1,ProducteurType.NUCLEAIRE,"Nantes");
		el17.addRuv(rv17);
		el17.addRuv(rv8);
		el17.addRuv(rv7);
	    GroupeProduction el4 = new GroupeProduction ("CIVAUX B1","CIVAU",1,ProducteurType.NUCLEAIRE,"Nantes");
		el4.addRuv(rv5);
		el4.addRuv(rv6);
		el4.addRuv(rv16);
	    GroupeProduction el5 = new GroupeProduction ("CIVAUX B2","CIVAU",2,ProducteurType.NUCLEAIRE,"Nantes");
	    el5.addRuv(rv5);
		el5.addRuv(rv6);
		el5.addRuv(rv16);
	    GroupeProduction el7 = new GroupeProduction ("CHINON B1","CHIN2",1,ProducteurType.NUCLEAIRE,"Nantes");
	    el7.addRuv(rv15);
		el7.addRuv(rv4);
		
	    GroupeProduction el8 = new GroupeProduction ("CHINON B2","CHIN2",2,ProducteurType.NUCLEAIRE,"Nantes");
	    el8.addRuv(rv15);
	  		el8.addRuv(rv4);
	  		
	    GroupeProduction el9 = new GroupeProduction ("CHINON B3","CHINX",3,ProducteurType.NUCLEAIRE,"Nantes");
	    el9.addRuv(rv15);
	  		el9.addRuv(rv4);
	  		
	    GroupeProduction el18 = new GroupeProduction ("CHINON B4","CHINX",4,ProducteurType.NUCLEAIRE,"Nantes");
	    el18.addRuv(rv15);
	  		el18.addRuv(rv4);
	  		
	    GroupeProduction el19 = new GroupeProduction ("ST LAURENT 1","SSEA2",1,ProducteurType.NUCLEAIRE,"Nantes");
	    el19.addRuv(rv18);
	  		el19.addRuv(rv11);
	  	  el19.addRuv(rv10);
			el19.addRuv(rv9);
			
	    GroupeProduction el20 = new GroupeProduction ("ST LAURENT 2","SSEA2",2,ProducteurType.NUCLEAIRE,"Nantes");
	    el20.addRuv(rv18);
  		el20.addRuv(rv11);
  	  el20.addRuv(rv10);
		el20.addRuv(rv9);
	    GroupeProduction el40 = new GroupeProduction ("BORT","BORT",ProducteurType.HYDRAULIQUE,"Toulouse");
	   // el40.setPoste_evacuation(el28);
	    GroupeProduction el41 = new GroupeProduction ("CHASTANG","CHAST",ProducteurType.HYDRAULIQUE,"Toulouse");
	   // el41.setPoste_evacuation(el11);
	    GroupeProduction el42 = new GroupeProduction ("MONTEZIC","MTEZI",ProducteurType.HYDRAULIQUE,"Toulouse");
	    //el42.setPoste_evacuation(el81);
	    GroupeProduction el43 = new GroupeProduction ("BROMMAT","BROMM",ProducteurType.HYDRAULIQUE,"Toulouse");
	   // el43.setPoste_evacuation(el80);
	    GroupeProduction el44 = new GroupeProduction ("AIGLE6","AIGLE",ProducteurType.HYDRAULIQUE,"Toulouse");
	    //el44.setPoste_evacuation(el28);
	    GroupeProduction el45 = new GroupeProduction ("BRENNILIS","BRENC",ProducteurType.THERMIQUE,"Nantes");
	    //el45.setPoste_evacuation(el78);
	    GroupeProduction el46 = new GroupeProduction ("ARRIGHI","ARRIG",ProducteurType.THERMIQUE,"St Quentin");
	    //el46.setPoste_evacuation(el24);
	    ////public DiagnostiqueProducteur(GroupeProduction id,String centre_exploitation,String type,String groupe, String etat, String ts, String commentaire)
 	  
	 
	    
	    //public RUV(GroupeProducturce,GroupeProduction target)
	    //RUV ruv1 = new RUV()
	    /*dao poste*/
		Dao.save(el1);
		Dao.save(el2);
		Dao.save(el3);
		Dao.save(el10);
		Dao.save(el11);
		
		Dao.save(el79);
		Dao.save(el80);
		Dao.save(el81);
		
		
	 	/*  DiagnostiqueProducteur dia2 = new DiagnostiqueProducteur(el19,"Nantes",ProducteurType.NUCLEAIRE,"ST LAURENT 1","iloté","TS1","hfurfufhfufbfu");
	 	   diaProd.save(dia2);
	 	  DiagnostiqueProducteur dia2 = new DiagnostiqueProducteur(el19,"Nantes",ProducteurType.NUCLEAIRE,"ST LAURENT 1","iloté","TS1","hfurfufhfufbfu");
	 	   diaProd.save(dia2);
	 	  DiagnostiqueProducteur dia2 = new DiagnostiqueProducteur(el19,"Nantes",ProducteurType.NUCLEAIRE,"ST LAURENT 1","iloté","TS1","hfurfufhfufbfu");
	 	   diaProd.save(dia2);*/
		/*DAO LIGNE.OUVRAGE*/
		 ouvragesDao.save(l1);
		 ouvragesDao.save(l2);
		 ouvragesDao.save(l3);
		 ouvragesDao.save(l4);
		 ouvragesDao.save(l5);
		 ouvragesDao.save(l6);
		 ouvragesDao.save(l7);
		 ouvragesDao.save(l8);
		 ouvragesDao.save(s1);
		 ouvragesDao.save(s2);
		 ouvragesDao.save(s3);
		 ouvragesDao.save(l9);
		 ouvragesDao.save(l10);
		 ouvragesDao.save(l11); ouvragesDao.save(l12); ouvragesDao.save(l13); ouvragesDao.save(l14);
		 ouvragesDao.save(l15); ouvragesDao.save(l16); ouvragesDao.save(l17); ouvragesDao.save(l18);
		 ouvragesDao.save(l20); ouvragesDao.save(l21); ouvragesDao.save(l22); ouvragesDao.save(l23);
		 ouvragesDao.save(l24); ouvragesDao.save(l25); ouvragesDao.save(l26); ouvragesDao.save(l27);
		 ouvragesDao.save(l28); /*ouvragesDao.save(l29);*/ ouvragesDao.save(l30); ouvragesDao.save(l31);
		 ouvragesDao.save(l32); ouvragesDao.save(l33); ouvragesDao.save(l34); ouvragesDao.save(l35); 
		 ouvragesDao.save(l36); ouvragesDao.save(l37); ouvragesDao.save(l38); ouvragesDao.save(l39);
		 ouvragesDao.save(l40); ouvragesDao.save(l41); ouvragesDao.save(l42); ouvragesDao.save(l43);
		 ouvragesDao.save(l44); ouvragesDao.save(l45); ouvragesDao.save(l46); ouvragesDao.save(l47);
		 ouvragesDao.save(l48); ouvragesDao.save(l49); ouvragesDao.save(l50); ouvragesDao.save(l51);
		 ouvragesDao.save(l52); ouvragesDao.save(l53); ouvragesDao.save(l54); ouvragesDao.save(l55);
		 /*ouvragesDao.save(l56);*/ ouvragesDao.save(l57); ouvragesDao.save(l58); ouvragesDao.save(l59);
		 ouvragesDao.save(l60); ouvragesDao.save(l61); ouvragesDao.save(l62); ouvragesDao.save(l63);
		 ouvragesDao.save(l64); ouvragesDao.save(l65); ouvragesDao.save(l66); ouvragesDao.save(l67);
		 ouvragesDao.save(l68); ouvragesDao.save(l69); ouvragesDao.save(l70); ouvragesDao.save(l71);
		 ouvragesDao.save(l72); ouvragesDao.save(l73); ouvragesDao.save(l74); ouvragesDao.save(l75);
		 ouvragesDao.save(l76); ouvragesDao.save(l77); ouvragesDao.save(l78); ouvragesDao.save(l79);
		 ouvragesDao.save(l80); ouvragesDao.save(ats1); ouvragesDao.save(ats2); ouvragesDao.save(ats3);
		 ouvragesDao.save(ats4); ouvragesDao.save(ats5); ouvragesDao.save(ats6); ouvragesDao.save(ats7);
		 ouvragesDao.save(at1); ouvragesDao.save(at2);ouvragesDao.save(l19); ouvragesDao.save(l81);
		 ouvragesDao.save(el1);
		 ouvragesDao.save(el2);
		 ouvragesDao.save(el3);
		 ouvragesDao.save(el10);
		 ouvragesDao.save(el11);
		 ouvragesDao.save(el24);	
		 ouvragesDao.save(el79);
		 ouvragesDao.save(el80);
		 ouvragesDao.save(el81);
			
		 /*save ruv*/
		    ruvDao.save(rv1);ruvDao.save(rv2);ruvDao.save(rv3);ruvDao.save(rv4);ruvDao.save(rv5);ruvDao.save(rv6);
		    ruvDao.save(rv7);ruvDao.save(rv8);ruvDao.save(rv9);ruvDao.save(rv10);ruvDao.save(rv11);ruvDao.save(rv12);
		    ruvDao.save(rv13); ruvDao.save(rv13); ruvDao.save(rv14); ruvDao.save(rv15); ruvDao.save(rv16); ruvDao.save(rv17); ruvDao.save(rv18);
		    diaReseau.save(dia10);
		 	diaReseau.save(dia11);
		 	diaReseau.save(dia12);
		 	diaReseau.save(dia13);
		 	diaReseau.save(dia14);
		 	diaReseau.save(dia15);
		 	diaReseau.save(dia16);
		 	diaReseau.save(dia17);
		 	diaReseau.save(dia18);
		 	diaReseau.save(dia19);
		 	diaReseau.save(dia20);
		 	diaReseau.save(dia21);
		 	diaReseau.save(dia22);
		 	diaReseau.save(dia23);
		 	diaReseau.save(dia24);
		 	diaReseau.save(dia25);
		 	diaReseau.save(dia26);
		 	diaReseau.save(dia27);
		 	diaReseau.save(dia28);
		 	diaReseau.save(dia29);
		 	//diaReseau.save(dia30);
		 	diaReseau.save(dia31);
		    /*dao groupe*/
			gproDao.save(el9);
			gproDao.save(el5);
			gproDao.save(el4);
			gproDao.save(el14);
			gproDao.save(el15);
			gproDao.save(el16);
			gproDao.save(el17);
			gproDao.save(el8);
			gproDao.save(el4);
			gproDao.save(el12);
			gproDao.save(el13);
			gproDao.save(el18);
			gproDao.save(el19);
			gproDao.save(el20);
			gproDao.save(el45);
			gproDao.save(el41);
			gproDao.save(el42);
			gproDao.save(el44);
			gproDao.save(el40);
			gproDao.save(el43);
			gproDao.save(el7);
			gproDao.save(el46);
			/* diaprod*/
			  DiagnostiqueProducteur dia1 = new DiagnostiqueProducteur(el19,"Nantes",ProducteurType.NUCLEAIRE,"ST LAURENT 1","declenché","TS2","hfurfufhfufbfu");
		 	    diaProd.save(dia1);
		 	  /*  DiagnostiqueProducteur dia2 = new DiagnostiqueProducteur(el42,"Toulouse",ProducteurType.HYDRAULIQUE,"MONTEZIC","iloté","TS1","hfurfufhfufbfu");
		 	   diaProd.save(dia2);*/
		 	  DiagnostiqueProducteur dia3 = new DiagnostiqueProducteur(el15,"Nantes",ProducteurType.NUCLEAIRE,"DAMPIERRE 3","iloté","TS2","hfurfufhfufbfu");
		 	   diaProd.save(dia3);
		 	  DiagnostiqueProducteur dia4 = new DiagnostiqueProducteur(el20,"Nantes",ProducteurType.NUCLEAIRE,"ST LAURENT 2","iloté","TS1","hfurfufhfufbfu");
		 	   diaProd.save(dia4);
		 	  DiagnostiqueProducteur dia5 = new DiagnostiqueProducteur(el9,"Nantes",ProducteurType.NUCLEAIRE,"CHINON B3 ","couplé","TS1","hfurfufhfufbfu");
		 	   diaProd.save(dia5);
		 	  DiagnostiqueProducteur dia6 = new DiagnostiqueProducteur(el17,"Nantes",ProducteurType.NUCLEAIRE,"DAMPIERRE 1","declenché","TS2P","hfurfufhfufbfu");
		 	   diaProd.save(dia6);
		 	  DiagnostiqueProducteur dia7 = new DiagnostiqueProducteur(el5,"Nantes",ProducteurType.NUCLEAIRE,"CIVAUX B2","iloté","TS2","hfurfufhfufbfu");
		 	   diaProd.save(dia7);
		 	  DiagnostiqueProducteur dia8 = new DiagnostiqueProducteur(el14,"Nantes",ProducteurType.NUCLEAIRE,"DAMPIERRE 2","iloté","NON TS1 TS2","hfurfufhfufbfu");
		 	   diaProd.save(dia8);
		 	  DiagnostiqueProducteur dia9 = new DiagnostiqueProducteur(el46,"St Quentin",ProducteurType.THERMIQUE,"ARRIGHI","couplé","TS1","hfurfufhfufbfu");
		 	   diaProd.save(dia9);
		//List <Gmr> g = new ArrayList<Gmr>();
		List <Gmr> g= c1.getGmrs();
		for (Gmr gf : g) {
			System.out.println("gdp :"+ gf.getLib_gmr());
		}
		//private GererPatrimoine gererPatrimoine;
	/*	List <GroupeProduction> gf1 = rv7.getGroup();
		for (GroupeProduction gfx : gf1) {
			System.out.println("nom du scenario "+rv7.getName()+": "+ gfx.getName());
		}*/
		System.out.println(el20.getTypeProd().toString());
		List<RUV> r = el19.getRuvs();
		for ( RUV rt: r){
			System.out.println("nom du scenario "+el19.getName()+": "+ rt.getName());	
		}
		//System.out.println(el9.getPoste_evacuation().getSite().getLib_site());
		//Dao.save(el6);
		spring.close();
		System.out.println("emun:"+ el1.getTension().getVolt());
		System.out.println(ats1.getNameOuvrage());
		System.out.println(l1.getGdp_chef_de_file().getLib_gdp());
		
		
	}

}
