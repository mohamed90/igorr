package fr.rte_france.igorr;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.rte_france.igorr.dao.CeDao;
import fr.rte_france.igorr.entity.Ce;

public class TestCe {
	public static void main(String[] args) throws Exception{
	ConfigurableApplicationContext spring = new ClassPathXmlApplicationContext("spring.xml");
	//GererCe gererCe = spring.getBean(GererCe.class);
	CeDao ceDao = spring.getBean(CeDao.class);

	//Créer objet
	List <Ce> listCe = new ArrayList<>();
	listCe.add(new Ce("Nantes"));
	listCe.add(new Ce("Toulouse"));
	listCe.add(new Ce("Saint Quentin"));
	listCe.add(new Ce("Lyon"));
	listCe.add(new Ce("Nancy"));
	listCe.add(new Ce("Marseille"));
	listCe.add(new Ce("Lille"));
	
	for (Ce ce : listCe) {
		ceDao.save(ce);
	}
	spring.close();
	}
}
