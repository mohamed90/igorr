package fr.rte_france.igorr;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.rte_france.igorr.dao.ProducteurDao;

import fr.rte_france.igorr.entity.Producteur;
import fr.rte_france.igorr.entity.ProducteurType;

public class TestProducteur {
	public static void main(String[] args) throws Exception{
		ConfigurableApplicationContext spring = new ClassPathXmlApplicationContext("spring.xml");
		ProducteurDao ProducteurDao = spring.getBean(ProducteurDao.class);

	//Créer objet
		Producteur element1 = new Producteur(1L, "CIVAU7CIVAU1", ProducteurType.NUCLEAIRE);
		/*element1.setX(4718.04);
		element1.setY(-21696.99);*/
		Producteur element2 = new Producteur(2L, "CHIN27CHIN22", ProducteurType.HYDRAULIQUE);
		/*element2.setX(4479.52);
		element2.setY(-22719.38);*/
		ProducteurDao.save(element1);
		ProducteurDao.save(element2);
		spring.close();
		System.out.println("emun:"+ element1.getType());
	}
}

