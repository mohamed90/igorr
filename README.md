# IGORR 2 (igorr-service)
## IGORR2

IGORR signifie `Incident Généralisé : Outil pour l'Optimisation de la Reconstitution du Réseau`

Cette outil destiné aux dispatchers, ainsi que pour la cellule de crise en cas d'incident généralisé (blackout d'une partie du réseau en France).

* IGORR2 permet de gérer  des consignes entièrement informatisées : édition, affichage, recherche, impression en PDF.
* Il permet d'aider le dispatcher à faire le diagnostic du réseau et de la production
* Il propose les consignes correspondantes à la situation diagnostiquée
* Il permet de partager les informations aux sein de l'équipe de façon transparente entre les CCO, le MCE et la cellule de crise
* Il affiche un schéma, une carte avec les informations importantes sur l'état du réseau dans cette situation (sans remplacer le SRC)
* Il permet aussi de tracer de les actions réalisées et donc l'avancement de la situation.

## igorr-service

Igorr-service est une application REST. Elle fournit un service pour accéder aux données contenues dans igorr.
Elle fonctionne avec l'interface utilisateur : igorr-web. Elle peut cependant servir d'autres applications.
